﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDrawingInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSalesForceNum = New System.Windows.Forms.Label()
        Me.txtSalesForce = New System.Windows.Forms.TextBox()
        Me.lblDrawingNum = New System.Windows.Forms.Label()
        Me.lblPage = New System.Windows.Forms.Label()
        Me.lblSubDir = New System.Windows.Forms.Label()
        Me.txtDrawingNum = New System.Windows.Forms.TextBox()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.txtSubDir = New System.Windows.Forms.TextBox()
        Me.btnStartDrawing = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblEvQuote = New System.Windows.Forms.Label()
        Me.lblCustPO = New System.Windows.Forms.Label()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblJobTag = New System.Windows.Forms.Label()
        Me.txtCustPO = New System.Windows.Forms.TextBox()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.txtEVQuote = New System.Windows.Forms.TextBox()
        Me.txtJobTag = New System.Windows.Forms.TextBox()
        Me.cbxDocType = New System.Windows.Forms.ComboBox()
        Me.lblDocType = New System.Windows.Forms.Label()
        Me.cbxRevision = New System.Windows.Forms.ComboBox()
        Me.lblRev = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblSalesForceNum
        '
        Me.lblSalesForceNum.AutoSize = True
        Me.lblSalesForceNum.Location = New System.Drawing.Point(51, 121)
        Me.lblSalesForceNum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSalesForceNum.Name = "lblSalesForceNum"
        Me.lblSalesForceNum.Size = New System.Drawing.Size(49, 17)
        Me.lblSalesForceNum.TabIndex = 0
        Me.lblSalesForceNum.Text = "OTT #"
        Me.lblSalesForceNum.UseWaitCursor = True
        '
        'txtSalesForce
        '
        Me.txtSalesForce.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalesForce.Location = New System.Drawing.Point(112, 117)
        Me.txtSalesForce.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSalesForce.Name = "txtSalesForce"
        Me.txtSalesForce.Size = New System.Drawing.Size(105, 22)
        Me.txtSalesForce.TabIndex = 6
        Me.txtSalesForce.UseWaitCursor = True
        '
        'lblDrawingNum
        '
        Me.lblDrawingNum.AutoSize = True
        Me.lblDrawingNum.Location = New System.Drawing.Point(35, 42)
        Me.lblDrawingNum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDrawingNum.Name = "lblDrawingNum"
        Me.lblDrawingNum.Size = New System.Drawing.Size(71, 17)
        Me.lblDrawingNum.TabIndex = 2
        Me.lblDrawingNum.Text = "Drawing #"
        Me.lblDrawingNum.UseWaitCursor = True
        '
        'lblPage
        '
        Me.lblPage.AutoSize = True
        Me.lblPage.Location = New System.Drawing.Point(195, 42)
        Me.lblPage.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Size = New System.Drawing.Size(41, 17)
        Me.lblPage.TabIndex = 3
        Me.lblPage.Text = "Page"
        Me.lblPage.UseWaitCursor = True
        '
        'lblSubDir
        '
        Me.lblSubDir.AutoSize = True
        Me.lblSubDir.Location = New System.Drawing.Point(315, 42)
        Me.lblSubDir.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSubDir.Name = "lblSubDir"
        Me.lblSubDir.Size = New System.Drawing.Size(55, 17)
        Me.lblSubDir.TabIndex = 4
        Me.lblSubDir.Text = "Sub Dir"
        Me.lblSubDir.UseWaitCursor = True
        '
        'txtDrawingNum
        '
        Me.txtDrawingNum.Location = New System.Drawing.Point(112, 38)
        Me.txtDrawingNum.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDrawingNum.Name = "txtDrawingNum"
        Me.txtDrawingNum.Size = New System.Drawing.Size(68, 22)
        Me.txtDrawingNum.TabIndex = 0
        Me.txtDrawingNum.UseWaitCursor = True
        '
        'txtPage
        '
        Me.txtPage.Location = New System.Drawing.Point(240, 38)
        Me.txtPage.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(59, 22)
        Me.txtPage.TabIndex = 1
        Me.txtPage.UseWaitCursor = True
        '
        'txtSubDir
        '
        Me.txtSubDir.Location = New System.Drawing.Point(372, 38)
        Me.txtSubDir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSubDir.Name = "txtSubDir"
        Me.txtSubDir.Size = New System.Drawing.Size(60, 22)
        Me.txtSubDir.TabIndex = 2
        Me.txtSubDir.UseWaitCursor = True
        '
        'btnStartDrawing
        '
        Me.btnStartDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnStartDrawing.Location = New System.Drawing.Point(95, 279)
        Me.btnStartDrawing.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnStartDrawing.Name = "btnStartDrawing"
        Me.btnStartDrawing.Size = New System.Drawing.Size(153, 28)
        Me.btnStartDrawing.TabIndex = 11
        Me.btnStartDrawing.Text = "&Start Drawing"
        Me.btnStartDrawing.UseVisualStyleBackColor = True
        Me.btnStartDrawing.UseWaitCursor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(283, 279)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.UseWaitCursor = True
        '
        'lblEvQuote
        '
        Me.lblEvQuote.AutoSize = True
        Me.lblEvQuote.Location = New System.Drawing.Point(232, 121)
        Me.lblEvQuote.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEvQuote.Name = "lblEvQuote"
        Me.lblEvQuote.Size = New System.Drawing.Size(85, 17)
        Me.lblEvQuote.TabIndex = 10
        Me.lblEvQuote.Text = "E.V. Quote#"
        Me.lblEvQuote.UseWaitCursor = True
        '
        'lblCustPO
        '
        Me.lblCustPO.AutoSize = True
        Me.lblCustPO.Location = New System.Drawing.Point(8, 160)
        Me.lblCustPO.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCustPO.Name = "lblCustPO"
        Me.lblCustPO.Size = New System.Drawing.Size(100, 17)
        Me.lblCustPO.TabIndex = 11
        Me.lblCustPO.Text = "Customer P.O."
        Me.lblCustPO.UseWaitCursor = True
        '
        'lblCustomer
        '
        Me.lblCustomer.AutoSize = True
        Me.lblCustomer.Location = New System.Drawing.Point(29, 201)
        Me.lblCustomer.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(68, 17)
        Me.lblCustomer.TabIndex = 13
        Me.lblCustomer.Text = "Customer"
        Me.lblCustomer.UseWaitCursor = True
        '
        'lblJobTag
        '
        Me.lblJobTag.AutoSize = True
        Me.lblJobTag.Location = New System.Drawing.Point(29, 244)
        Me.lblJobTag.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJobTag.Name = "lblJobTag"
        Me.lblJobTag.Size = New System.Drawing.Size(60, 17)
        Me.lblJobTag.TabIndex = 14
        Me.lblJobTag.Text = "Job Tag"
        Me.lblJobTag.UseWaitCursor = True
        '
        'txtCustPO
        '
        Me.txtCustPO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCustPO.Location = New System.Drawing.Point(112, 156)
        Me.txtCustPO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCustPO.Name = "txtCustPO"
        Me.txtCustPO.Size = New System.Drawing.Size(221, 22)
        Me.txtCustPO.TabIndex = 8
        Me.txtCustPO.UseWaitCursor = True
        '
        'txtCustomer
        '
        Me.txtCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCustomer.Location = New System.Drawing.Point(112, 197)
        Me.txtCustomer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(321, 22)
        Me.txtCustomer.TabIndex = 9
        Me.txtCustomer.UseWaitCursor = True
        '
        'txtEVQuote
        '
        Me.txtEVQuote.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEVQuote.Location = New System.Drawing.Point(328, 117)
        Me.txtEVQuote.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtEVQuote.Name = "txtEVQuote"
        Me.txtEVQuote.Size = New System.Drawing.Size(105, 22)
        Me.txtEVQuote.TabIndex = 7
        Me.txtEVQuote.UseWaitCursor = True
        '
        'txtJobTag
        '
        Me.txtJobTag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJobTag.Location = New System.Drawing.Point(112, 240)
        Me.txtJobTag.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtJobTag.Name = "txtJobTag"
        Me.txtJobTag.Size = New System.Drawing.Size(321, 22)
        Me.txtJobTag.TabIndex = 10
        Me.txtJobTag.UseWaitCursor = True
        '
        'cbxDocType
        '
        Me.cbxDocType.Enabled = False
        Me.cbxDocType.FormattingEnabled = True
        Me.cbxDocType.Items.AddRange(New Object() {"Specials Drawings", "Shop Drawings"})
        Me.cbxDocType.Location = New System.Drawing.Point(112, 78)
        Me.cbxDocType.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxDocType.Name = "cbxDocType"
        Me.cbxDocType.Size = New System.Drawing.Size(183, 24)
        Me.cbxDocType.TabIndex = 4
        Me.cbxDocType.Text = "Specials Drawings"
        Me.cbxDocType.UseWaitCursor = True
        '
        'lblDocType
        '
        Me.lblDocType.AutoSize = True
        Me.lblDocType.Location = New System.Drawing.Point(29, 81)
        Me.lblDocType.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDocType.Name = "lblDocType"
        Me.lblDocType.Size = New System.Drawing.Size(73, 17)
        Me.lblDocType.TabIndex = 18
        Me.lblDocType.Text = "Doc. Type"
        Me.lblDocType.UseWaitCursor = True
        '
        'cbxRevision
        '
        Me.cbxRevision.Enabled = False
        Me.cbxRevision.FormattingEnabled = True
        Me.cbxRevision.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y"})
        Me.cbxRevision.Location = New System.Drawing.Point(372, 78)
        Me.cbxRevision.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxRevision.Name = "cbxRevision"
        Me.cbxRevision.Size = New System.Drawing.Size(61, 24)
        Me.cbxRevision.TabIndex = 5
        Me.cbxRevision.Text = "A"
        Me.cbxRevision.UseWaitCursor = True
        '
        'lblRev
        '
        Me.lblRev.AutoSize = True
        Me.lblRev.Location = New System.Drawing.Point(304, 81)
        Me.lblRev.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRev.Name = "lblRev"
        Me.lblRev.Size = New System.Drawing.Size(62, 17)
        Me.lblRev.TabIndex = 20
        Me.lblRev.Text = "Revision"
        Me.lblRev.UseWaitCursor = True
        '
        'frmDrawingInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(505, 332)
        Me.Controls.Add(Me.lblRev)
        Me.Controls.Add(Me.cbxRevision)
        Me.Controls.Add(Me.lblDocType)
        Me.Controls.Add(Me.cbxDocType)
        Me.Controls.Add(Me.txtJobTag)
        Me.Controls.Add(Me.txtEVQuote)
        Me.Controls.Add(Me.txtCustomer)
        Me.Controls.Add(Me.txtCustPO)
        Me.Controls.Add(Me.lblJobTag)
        Me.Controls.Add(Me.lblCustomer)
        Me.Controls.Add(Me.lblCustPO)
        Me.Controls.Add(Me.lblEvQuote)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnStartDrawing)
        Me.Controls.Add(Me.txtSubDir)
        Me.Controls.Add(Me.txtPage)
        Me.Controls.Add(Me.txtDrawingNum)
        Me.Controls.Add(Me.lblSubDir)
        Me.Controls.Add(Me.lblPage)
        Me.Controls.Add(Me.lblDrawingNum)
        Me.Controls.Add(Me.txtSalesForce)
        Me.Controls.Add(Me.lblSalesForceNum)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmDrawingInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Drawing Titleblock Information"
        Me.UseWaitCursor = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSalesForceNum As System.Windows.Forms.Label
    Friend WithEvents txtSalesForce As System.Windows.Forms.TextBox
    Friend WithEvents lblDrawingNum As System.Windows.Forms.Label
    Friend WithEvents lblPage As System.Windows.Forms.Label
    Friend WithEvents lblSubDir As System.Windows.Forms.Label
    Friend WithEvents txtDrawingNum As System.Windows.Forms.TextBox
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents txtSubDir As System.Windows.Forms.TextBox
    Friend WithEvents btnStartDrawing As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblEvQuote As System.Windows.Forms.Label
    Friend WithEvents lblCustPO As System.Windows.Forms.Label
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblJobTag As System.Windows.Forms.Label
    Friend WithEvents txtCustPO As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtEVQuote As System.Windows.Forms.TextBox
    Friend WithEvents txtJobTag As System.Windows.Forms.TextBox
    Friend WithEvents cbxDocType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocType As System.Windows.Forms.Label
    Friend WithEvents cbxRevision As System.Windows.Forms.ComboBox
    Friend WithEvents lblRev As System.Windows.Forms.Label
End Class
