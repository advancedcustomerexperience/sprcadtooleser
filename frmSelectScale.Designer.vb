﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectScale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.optSC1 = New System.Windows.Forms.RadioButton()
        Me.optSC2 = New System.Windows.Forms.RadioButton()
        Me.optSC3 = New System.Windows.Forms.RadioButton()
        Me.optSC4 = New System.Windows.Forms.RadioButton()
        Me.optSC5 = New System.Windows.Forms.RadioButton()
        Me.optSC6 = New System.Windows.Forms.RadioButton()
        Me.optSC7 = New System.Windows.Forms.RadioButton()
        Me.optSC8 = New System.Windows.Forms.RadioButton()
        Me.optSC9 = New System.Windows.Forms.RadioButton()
        Me.optSC10 = New System.Windows.Forms.RadioButton()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'optSC1
        '
        Me.optSC1.AutoSize = True
        Me.optSC1.Location = New System.Drawing.Point(45, 10)
        Me.optSC1.Name = "optSC1"
        Me.optSC1.Size = New System.Drawing.Size(70, 17)
        Me.optSC1.TabIndex = 0
        Me.optSC1.TabStop = True
        Me.optSC1.Text = "6"" = 1'-0"""
        Me.optSC1.UseVisualStyleBackColor = True
        '
        'optSC2
        '
        Me.optSC2.AutoSize = True
        Me.optSC2.Location = New System.Drawing.Point(45, 33)
        Me.optSC2.Name = "optSC2"
        Me.optSC2.Size = New System.Drawing.Size(70, 17)
        Me.optSC2.TabIndex = 1
        Me.optSC2.TabStop = True
        Me.optSC2.Text = "3"" = 1'-0"""
        Me.optSC2.UseVisualStyleBackColor = True
        '
        'optSC3
        '
        Me.optSC3.AutoSize = True
        Me.optSC3.Location = New System.Drawing.Point(45, 56)
        Me.optSC3.Name = "optSC3"
        Me.optSC3.Size = New System.Drawing.Size(70, 17)
        Me.optSC3.TabIndex = 2
        Me.optSC3.TabStop = True
        Me.optSC3.Text = "2"" = 1'-0"""
        Me.optSC3.UseVisualStyleBackColor = True
        '
        'optSC4
        '
        Me.optSC4.AutoSize = True
        Me.optSC4.Location = New System.Drawing.Point(45, 79)
        Me.optSC4.Name = "optSC4"
        Me.optSC4.Size = New System.Drawing.Size(90, 17)
        Me.optSC4.TabIndex = 3
        Me.optSC4.TabStop = True
        Me.optSC4.Text = "1 1/2"" = 1'-0"""
        Me.optSC4.UseVisualStyleBackColor = True
        '
        'optSC5
        '
        Me.optSC5.AutoSize = True
        Me.optSC5.Location = New System.Drawing.Point(45, 102)
        Me.optSC5.Name = "optSC5"
        Me.optSC5.Size = New System.Drawing.Size(70, 17)
        Me.optSC5.TabIndex = 4
        Me.optSC5.TabStop = True
        Me.optSC5.Text = "1"" = 1'-0"""
        Me.optSC5.UseVisualStyleBackColor = True
        '
        'optSC6
        '
        Me.optSC6.AutoSize = True
        Me.optSC6.Location = New System.Drawing.Point(45, 125)
        Me.optSC6.Name = "optSC6"
        Me.optSC6.Size = New System.Drawing.Size(81, 17)
        Me.optSC6.TabIndex = 5
        Me.optSC6.TabStop = True
        Me.optSC6.Text = "3/4"" = 1'-0"""
        Me.optSC6.UseVisualStyleBackColor = True
        '
        'optSC7
        '
        Me.optSC7.AutoSize = True
        Me.optSC7.Location = New System.Drawing.Point(45, 148)
        Me.optSC7.Name = "optSC7"
        Me.optSC7.Size = New System.Drawing.Size(81, 17)
        Me.optSC7.TabIndex = 6
        Me.optSC7.TabStop = True
        Me.optSC7.Text = "1/2"" = 1'-0"""
        Me.optSC7.UseVisualStyleBackColor = True
        '
        'optSC8
        '
        Me.optSC8.AutoSize = True
        Me.optSC8.Location = New System.Drawing.Point(45, 171)
        Me.optSC8.Name = "optSC8"
        Me.optSC8.Size = New System.Drawing.Size(81, 17)
        Me.optSC8.TabIndex = 7
        Me.optSC8.TabStop = True
        Me.optSC8.Text = "3/8"" = 1'-0"""
        Me.optSC8.UseVisualStyleBackColor = True
        '
        'optSC9
        '
        Me.optSC9.AutoSize = True
        Me.optSC9.Location = New System.Drawing.Point(45, 194)
        Me.optSC9.Name = "optSC9"
        Me.optSC9.Size = New System.Drawing.Size(87, 17)
        Me.optSC9.TabIndex = 8
        Me.optSC9.TabStop = True
        Me.optSC9.Text = "5/16"" = 1'-0"""
        Me.optSC9.UseVisualStyleBackColor = True
        '
        'optSC10
        '
        Me.optSC10.AutoSize = True
        Me.optSC10.Location = New System.Drawing.Point(45, 217)
        Me.optSC10.Name = "optSC10"
        Me.optSC10.Size = New System.Drawing.Size(81, 17)
        Me.optSC10.TabIndex = 9
        Me.optSC10.TabStop = True
        Me.optSC10.Text = "1/4"" = 1'-0"""
        Me.optSC10.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(92, 241)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(59, 23)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(27, 241)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(59, 23)
        Me.btnOK.TabIndex = 11
        Me.btnOK.Text = "Continue"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'frmSelectScale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(172, 276)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.optSC10)
        Me.Controls.Add(Me.optSC9)
        Me.Controls.Add(Me.optSC8)
        Me.Controls.Add(Me.optSC7)
        Me.Controls.Add(Me.optSC6)
        Me.Controls.Add(Me.optSC5)
        Me.Controls.Add(Me.optSC4)
        Me.Controls.Add(Me.optSC3)
        Me.Controls.Add(Me.optSC2)
        Me.Controls.Add(Me.optSC1)
        Me.Name = "frmSelectScale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Scale Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents optSC1 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC2 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC3 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC4 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC5 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC6 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC7 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC8 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC9 As System.Windows.Forms.RadioButton
    Friend WithEvents optSC10 As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
End Class
