﻿' (C) Copyright 2011 by  
'
Imports System
Imports System.IO
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Autodesk.AutoCAD.Runtime
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.AutoCAD.Interop.Common
Imports Autodesk.AutoCAD.DatabaseServices
Imports Autodesk.AutoCAD.Geometry
Imports Autodesk.AutoCAD.EditorInput
Imports Autodesk.AutoCAD.Windows
Imports Autodesk.AutoCAD.ApplicationServices.DocumentExtension
Imports Autodesk.AutoCAD.ApplicationServices.DocumentCollectionExtension
Imports Autodesk.AutoCAD.Interop







' This line is not mandatory, but improves loading performances
<Assembly: CommandClass(GetType(E_Series_SPR_CAD_Cutomization.MyCommands))> 
Namespace E_Series_SPR_CAD_Cutomization

    ' This class is instantiated by AutoCAD for each document when
    ' a command is called by the user the first time in the context
    ' of a given document. In other words, non static data in this class
    ' is implicitly per-document!
    Public Class MyCommands
        Public NewDraw As Boolean
        Public newDrawNum As Boolean
        Public LayoutFlag As Boolean = False
        Public strOTTNumber As String
        Public strDrawingScale As String
        Public strJobTag As String
        Public strCustomerName As String
        Public strDrawingNumber As String
        Public strRevNumber As String
        Public strQuoteNumber As String
        Public strPurchaseOrder As String
        Public strPageNumber As String
        Public strUserName As String
        Public strDWGDate As String
        Public strDrawingType As String
        Public strSubDirectory As String
        Public AddDrawingToJob As Boolean
        Public strScale As String
        Public revlevel As String
        Public revdrafter As String
        Public revdate As String
        Public RevTag As String
        Public revsalesforce As String
        Public revDrawingNum As String
        Public drawingNum As String
        Public mydrawing As Autodesk.AutoCAD.ApplicationServices.Document

        Public mydoc As Document = Application.DocumentManager.MdiActiveDocument
        'Public pathString As String = "R:\Engineering - Eagle\Public\"
        Public pathString As String = "\\andersencorp.com\groups\Engineering - Eagle\Public\"
        Public SprSupportPathString As String = "C:\ProgramData\Autodesk\SPR Support Files"
        Public UserName As String
        Public MfgRev As String
        Public myDrawingNumber As String

        'Public myopendocument As AcadDocument

        <CommandMethod("SPR", CommandFlags.Session)> _
        Public Sub SPR()
            Dim myStartUpForm As New frmStartUp

            Try
                Select Case Application.ShowModalDialog(myStartUpForm)
                    Case System.Windows.Forms.DialogResult.OK
                        If myStartUpForm.NewDrawing = "Yes" Then ' New drawing was selected in startup
                            'call function new drawing
                            AddDrawingToJob = False
                            NewDrawing("A", GetNewNumber()) 'Updated
                        ElseIf myStartUpForm.AddDrawing = "Yes" Then 'Add drawing was selected in startup
                            'call function Add Drawing
                            AddDrawingToJob = True
                            AddDrawing() 'Updated
                        ElseIf myStartUpForm.ApproveDrawing = "Yes" Then
                            ApprovedDrawing()
                        ElseIf myStartUpForm.SignoffRevision = "Yes" Then
                            'MsgBox("Drawing Revision")
                            SignoffRevision()

                        ElseIf myStartUpForm.OpenDrawing = "Yes" Then
                            'MsgBox("Open drawing")
                            openexistingdrawing()
                        ElseIf myStartUpForm.CloseDrawing = "Yes" Then
                            'MsgBox("Close Drawing")
                            CloseDrawing()
                        ElseIf myStartUpForm.CopyLyout = "Yes" Then
                            'MsgBox("Close Drawing")
                            CopyLayout() 'Updated 2/3/17 SCT
                        ElseIf myStartUpForm.MoveAppFile = "Yes" Then
                            'MsgBox("Update Title")
                            'ReadElevationTitleBlocks()
                            'getdrawinginfo(strDrawingNumber, getSubDir(strDrawingNumber), strPageNumber)
                            'UpdateSignoffTitleBlocks()
                            MoveApprovedFile()
                        ElseIf myStartUpForm.IQApproval = "Yes" Then
                            'MsgBox("Close Drawing")
                            IQApproval() 'Added new 9/28/18
                        ElseIf myStartUpForm.IQOpenDrawing = "Yes" Then
                            'MsgBox("Close Drawing")
                            IQOpenDrawing() 'Added new 9/28/18
                        ElseIf myStartUpForm.IQCopyDrawing = "Yes" Then
                            'MsgBox("Close Drawing")
                            IQCopyDrawing() 'Added new 9/28/18
                        End If
                    Case System.Windows.Forms.DialogResult.Cancel
                        'MsgBox("Canceled Program")

                End Select
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught in startup:" & _
                                          vbLf & ex.Message)

            End Try
        End Sub
        Public Function GetNewNumber()
            Dim objFile As New System.IO.StreamReader(pathString & "SPR Support Files\NextDrawingNumber.txt")
            Dim myNewDrawingNumber As String = ""
            Dim DrawingNumOut As String
            Dim myNewNumber As Double

            'Check to see if next drawing number file is available
            Dim objFileNextDrawingNumber As Boolean
            objFileNextDrawingNumber = System.IO.File.Exists(pathString & "SPR Support Files\NextDrawingNumber.txt")
            If objFileNextDrawingNumber = True Then
                'do nothing
            Else
                'Next Drawing number is missing
                MsgBox("the next drawing file is missing, contact the CAD administrator")
            End If
            If AddDrawingToJob = False Then
                'get new drawing number
                myNewDrawingNumber = objFile.ReadToEnd
                objFile.Close()
                objFile.Dispose()
                newDrawNum = True
                'Current draing number plus 1 equals next number
                If Left(myNewDrawingNumber, 1) = "0" Then
                    myNewNumber = Convert.ToDouble(myNewDrawingNumber) + 1
                    If myNewNumber < 100000 Then
                        DrawingNumOut = "0" & myNewNumber
                    Else
                        DrawingNumOut = Convert.ToString(myNewNumber)
                    End If
                Else
                    DrawingNumOut = Convert.ToString(Convert.ToDouble(myNewDrawingNumber) + 1)
                End If
                'Write next drawing number to NextDrawing Number.txt
                Dim objFile2 As New System.IO.StreamWriter(pathString & "SPR Support Files\NextDrawingNumber.txt")
                objFile2.Write(DrawingNumOut)
                objFile2.Close()
                objFile.Dispose()
            Else

                myNewDrawingNumber = Left(Application.GetSystemVariable("DWGNAME"), 6)
            End If


            Return myNewDrawingNumber
        End Function
        Public Sub NewDrawing(ByVal newPageNumber As String, ByVal newDrawingNumber As String)
            'Update sub 2/4/17 to accomodate new title blocks
            Dim NewSubDirectory As String = getSubDir(newDrawingNumber)
            strRevNumber = "1"
            strDrawingType = "Specials Drawings"
            strDrawingNumber = newDrawingNumber
            strPageNumber = newPageNumber
            myDrawingNumber = newDrawingNumber & newPageNumber

            If AddDrawingToJob = True Then
                ReadDrawingInfo()
            End If
            'Go get OTT number
            GetOTTNumber()
            'Does non-approved path exist
            NonApprovedPathExist(newDrawingNumber)
            'If file exist warn user and exit sub       
            If NonApprovedFileExist(newDrawingNumber) = True Then
                'file alread exist
                MsgBox("File already exist, will exit sub")
                Exit Sub
            Else
                'Create new drawing
                NewSpecDrawing()
            End If
            'Update signoff title blocks
            UpdateSignoffTitleBlocks()
            MfgRev = "P-1"
            RevTag = "Pre-Production"
            'Update supplier title blocks
            UpdateSupplierTitleBlocks()

            WriteDrawingInfo(NewSubDirectory)
            'switch to model layout
            switchlayout("Model")
            'Save active file prior to drawing
            Dim newFileName As String = pathString & "Specials Drawings\NonApproved\" & (getSubDir(newDrawingNumber)) & "\" & newDrawingNumber & newPageNumber & ".dwg"
            SaveActiveDrawing(newFileName)
            strOTTNumber = Nothing
            strRevNumber = Nothing
        End Sub
        Public Sub AddDrawing()
            Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
            ReadSignoffTitleBlock()
            WriteDrawingInfo(getSubDir(strDrawingNumber))
            switchlayout("Signoff P")
            cdoc.SendStringToExecute("._QSAVE ", True, False, False)
            NewDrawing(GetNewPage(), GetNewNumber())
        End Sub

        Public Function getSubDir(ByVal drawingNum As String) As String
            Dim Subright As String
            Dim Subextension As String = ""
            Dim NewSubDir As String

            If Len(drawingNum) = 12 Then
                NewSubDir = Microsoft.VisualBasic.Left(drawingNum, 6) & "00"
                Return NewSubDir
            ElseIf Len(drawingNum) = 13 Then
                NewSubDir = Microsoft.VisualBasic.Left(drawingNum, 7) & "00"
                Return NewSubDir
                Else
                'Get right two of drawing number
                Subright = Microsoft.VisualBasic.Right(drawingNum, 2)
                'determine sub
                If Convert.ToDouble(Subright) >= 75 Then
                    Subextension = "75"
                ElseIf Convert.ToDouble(Subright) >= 50 Then
                    Subextension = "50"
                ElseIf Convert.ToDouble(Subright) >= 25 Then
                    Subextension = "25"
                ElseIf Convert.ToDouble(Subright) >= 0 Then
                    Subextension = "00"
                Else
                    MsgBox("System could not determine the correct sub directory", )
                End If
                NewSubDir = Microsoft.VisualBasic.Left(drawingNum, 4) & Subextension
                Return NewSubDir
            End If


           


        End Function



        ' LispFunction is similar to CommandMethod but it creates a lisp 
        ' callable function. Many return types are supported not just string
        ' or integer.
        <LispFunction("MyLispFunction", "MyLispFunctionLocal")> _
        Public Function MyLispFunction(ByVal args As ResultBuffer) ' This method can have any name
            ' Put your command code here

            ' Return a value to the AutoCAD Lisp Interpreter
            Return 1
        End Function
        Public Sub GetOTTNumber()
            Dim myOTTNumber As New frmOTTNUmber
            myOTTNumber.txtOTTNumber.Text = strOTTNumber
            Select Case Application.ShowModalDialog(myOTTNumber)
                Case System.Windows.Forms.DialogResult.OK
                    'Set public variables              
                    strOTTNumber = myOTTNumber.txtOTTNumber.Text
                    strDWGDate = GetDate()
                    strUserName = GetDrafter()
                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub


            End Select
            'Old process 1/26/17 for old title blocks

            ''Open Drawing Information dialog 
            'Select Case Application.ShowModalDialog(myDrawingInfo)
            '    Case System.Windows.Forms.DialogResult.OK
            '        'Set public variables              
            '        strOTTNumber = myDrawingInfo.txtSalesForce.Text
            '        strDrawingScale = " "
            '        strJobTag = myDrawingInfo.txtJobTag.Text
            '        strCustomerName = myDrawingInfo.txtCustomer.Text
            '        strDrawingNumber = myDrawingInfo.txtDrawingNum.Text
            '        strRevNumber = myDrawingInfo.cbxRevision.Text
            '        strQuoteNumber = myDrawingInfo.txtEVQuote.Text
            '        strPurchaseOrder = myDrawingInfo.txtCustPO.Text
            '        strPageNumber = myDrawingInfo.txtPage.Text
            '        strDrawingType = myDrawingInfo.cbxDocType.Text
            '        strDWGDate = GetDate()
            '        strUserName = GetDrafter()

            '        'Write drawing information to file for future use
            '        'Fix Code Below
            '        'WriteDrawingInfo()
            '    Case System.Windows.Forms.DialogResult.Cancel
            '        Exit Sub


            'End Select


        End Sub

        Public Function GetDrafter()
            Dim UserName As String
            Dim strUserName As String

            UserName = GetUserName()

            If UserName = "a29638" Then
                'Stan Thomas
                strUserName = "SCT"
                UserName = "Stan Thomas"
            ElseIf UserName = "a29527" Then
                'Tony Nies
                strUserName = "TWN"
            ElseIf UserName = "a30648" Then
                'Kathy Smith
                strUserName = "KJS"
            ElseIf UserName = "a55355" Then
                'Maranda Runde
                strUserName = "MLR"
            ElseIf UserName = "a33053" Then
                'Tim Pfile
                strUserName = "JM"
            ElseIf UserName = "a64244" Then
                'Jeff Mitchell
                strUserName = "JDM"
            ElseIf UserName = "a30181" Then
                'Chris Frederick
                strUserName = "CJF"
            ElseIf UserName = "a31385" Then
                'Bill Batts
                strUserName = "WBB"
            ElseIf UserName = "a55773" Then
                'Scott Hamilton
                strUserName = "SBH"
            ElseIf UserName = "a61741" Then
                'Gary Juno
                strUserName = "GDJ"
            ElseIf UserName = "a68230" Then
                'Jeremy Henkes
                strUserName = "JRH"
            ElseIf UserName = "a68817" Then
                'Thomas Coffman
                strUserName = "TRC"
            ElseIf UserName = "a29935" Then
                'Crystal Sand
                strUserName = "CLS"
            ElseIf UserName = "a61352" Then
                'Scott Wagner
                strUserName = "SMW"
            ElseIf UserName = "a58200" Then
                'Cole Fenner
                strUserName = "CMF"
            ElseIf UserName = "a56917" Then
                'Beverly Bellows
                strUserName = "BAB"
            ElseIf UserName = "a29404" Then
                'Robert Lee
                strUserName = "REL"
            ElseIf UserName = "a29493" Then
                'Shannon Belken
                strUserName = "SMB"
            ElseIf UserName = "a29452" Then
                'Mark Kilgore
                strUserName = "MAK"

            ElseIf UserName = "a70832" Then
                'Angela Wittmer
                strUserName = "ADW"

            Else
                strUserName = UserName
            End If

            GetDrafter = strUserName
            Return strUserName
            ' Display the User Name

            'MsgBox "User is: " & strUserName
        End Function
        Declare Function GetUserName Lib "advapi32.dll" Alias _
           "GetUserNameA" (ByVal lpBuffer As String, _
           ByRef nSize As Integer) As Integer

        Public Function GetUserName() As String
            Dim iReturn As Integer
            Dim userName As String
            userName = New String(CChar(" "), 50)
            iReturn = GetUserName(userName, 50)
            GetUserName = userName.ToLower.Substring(0, userName.IndexOf(Chr(0)))
        End Function

        Public Function GetDate()
            Dim date1 As Date = Date.Now

            ' Get date-only portion of date, without its time.
            Dim dateOnly As Date = date1.Date
            GetDate = dateOnly.ToString("d")

        End Function
        Public Sub NonApprovedPathExist(ByVal DrawingNumber As String)
            Dim FilePath As String = pathString & "Specials Drawings\NonApproved\" & getSubDir(Left(DrawingNumber, 6))

            'Check to see if sub dirrectory folder exists on network
            Dim folderexist As Boolean
            folderexist = System.IO.Directory.Exists(FilePath)
            If folderexist = True Then
                'Sub directory already exist, do nothing
            Else
                'Create Specials drawing sub folder if is does not already exist.
                System.IO.Directory.CreateDirectory(FilePath)
            End If

        End Sub
        Public Sub ApprovedPathExist(ByVal DrawingNumber As String)
            Dim FilePath As String = pathString & "Specials Drawings\Approved\" & getSubDir(Left(DrawingNumber, 6))

            'Check to see if sub dirrectory folder exists on network
            Dim folderexist As Boolean
            folderexist = System.IO.Directory.Exists(FilePath)
            If folderexist = True Then
                'Sub directory already exist, do nothing
            Else
                'Create Specials drawing sub folder if is does not already exist.
                System.IO.Directory.CreateDirectory(FilePath)
            End If

        End Sub
        Public Sub IQApprovedPathExist(ByVal DrawingNumber As String)
            Dim FilePath As String = pathString & "Specials Drawings\Approved\" & getSubDir(DrawingNumber)

            'Check to see if sub dirrectory folder exists on network
            Dim folderexist As Boolean
            folderexist = System.IO.Directory.Exists(FilePath)
            If folderexist = True Then
                'Sub directory already exist, do nothing
            Else
                'Create Specials drawing sub folder if is does not already exist.
                System.IO.Directory.CreateDirectory(FilePath)
            End If

        End Sub

        Public Function ApprovedFileExist(ByVal DrawingNumber As String)
            Dim AppFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(Left(DrawingNumber, 6)) & "\" & DrawingNumber & ".dwg"

            'Check to see if sub dirrectory folder exists on network
            Dim File As Boolean
            File = System.IO.File.Exists(AppFile)
            Return File
        End Function
        Public Function NonApprovedFileExist(ByVal DrawingNumber As String)
            Dim NonAppFile As String = pathString & "Specials Drawings\NonApproved\" & getSubDir(Left(DrawingNumber, 6)) & "\" & DrawingNumber & ".dwg"

            'Check to see if sub dirrectory folder exists on network
            Dim File As Boolean
            File = System.IO.File.Exists(NonAppFile)
            Return File
        End Function
        Public Function NonApprovedLockFileExist(ByVal DrawingNumber As String)
            Dim NonAppFile As String = pathString & "Specials Drawings\NonApproved\" & getSubDir(Left(DrawingNumber, 6)) & "\" & DrawingNumber & ".dwl"

            'Check to see if sub dirrectory folder exists on network
            Dim File As Boolean
            File = System.IO.File.Exists(NonAppFile)
            Return File
        End Function
        Public Sub openexistingdrawing()
            ' Dim NonApprovedFilePath As String
            Dim ApprovedFilePath As String

            Dim myGetDrawingForm As New frmGetDrawingNum
            'Dim addNewRev As Integer
            Dim addAppNewRev As Boolean = False
            newDrawNum = False
            ' app.ActiveDocument.Close()
            Select Case Application.ShowModalDialog(myGetDrawingForm)
                Case System.Windows.Forms.DialogResult.OK
                    Dim openDrawingNumber As String = myGetDrawingForm.txtGetDrawing.Text
                    myDrawingNumber = openDrawingNumber

                    'WriteDrawingApp()
                    ApprovedFilePath = pathString & "Specials Drawings\Approved\" & getSubDir(Left(openDrawingNumber, 6)) & "\" & openDrawingNumber & ".dwg"
                    'Check to see if file exists in the proper non-approved folder
                    If NonApprovedFileExist(openDrawingNumber) = True And ApprovedFileExist(openDrawingNumber) = True Then
                        MsgBox("Drawing " & myGetDrawingForm.txtGetDrawing.Text & " is in both the approved and non-approved folders. Verify which is the corrrect drawing")
                    ElseIf NonApprovedFileExist(openDrawingNumber) = True Then
                        'Check to see if drawing is already open. Added by stan thomas 6/14/16
                        If NonApprovedLockFileExist(openDrawingNumber) = True Then
                            'Determine user of lock file
                            Dim LockFile As String = pathString & "Specials Drawings\NonApproved\" & getSubDir(Left(openDrawingNumber, 6)) & "\" & openDrawingNumber & ".dwl"
                            Dim fs As FileSecurity = File.GetAccessControl(LockFile)
                            Dim sid As IdentityReference = fs.GetOwner(GetType(SecurityIdentifier))
                            Dim ntaccount As IdentityReference = sid.Translate(GetType(NTAccount))
                            Dim owner As String = ntaccount.ToString()
                            MsgBox("File is already open by " & owner & ".")
                        Else
                            'No lock file found so open drawing for edit.
                            OpenDrawing(pathString & "Specials Drawings\NonApproved\" & getSubDir(Left(openDrawingNumber, 6)) & "\" & openDrawingNumber & ".dwg")
                        End If

                    ElseIf ApprovedFileExist(openDrawingNumber) = True Then
                        MsgBox("Drawing is already approved, no changes can be made. Create a new drawing with the requested changes. Will open read only.")
                        OpenDrawingReadOnly(ApprovedFilePath)
                    Else
                        MsgBox(myGetDrawingForm.txtGetDrawing.Text & ".dwg does not exist in either Non-Approved or Approved, Check Drawing Number")
                        'CreateNewDwg()
                    End If
                Case System.Windows.Forms.DialogResult.Cancel
            End Select
        End Sub
        Public Sub SaveActiveDrawing(ByVal strDWGName As String)
            Dim acDoc As Document = Application.DocumentManager.MdiActiveDocument
            acDoc.Database.SaveAs(strDWGName, True, DwgVersion.Current, acDoc.Database.SecurityParameters)

        End Sub
        Public Sub OpenDrawing(ByVal strFileName As String)
            ' Dim strFileName As String = "C:\campus.dwg"

            Dim acDocMgr As DocumentCollection = Application.DocumentManager

            If (File.Exists(strFileName)) Then
                DocumentCollectionExtension.Open(acDocMgr, strFileName, False)
            Else
                acDocMgr.MdiActiveDocument.Editor.WriteMessage("File " & strFileName & _
                                                               " does not exist.")
            End If
            ReadSignoffTitleBlock()

            WriteDrawingInfo(getSubDir(strDrawingNumber))
        End Sub
        Public Sub OpenDrawingReadOnly(ByVal strFileName As String)
            ' Dim strFileName As String = "C:\campus.dwg"

            Dim acDocMgr As DocumentCollection = Application.DocumentManager

            If (File.Exists(strFileName)) Then
                DocumentCollectionExtension.Open(acDocMgr, strFileName, True)
            Else
                acDocMgr.MdiActiveDocument.Editor.WriteMessage("File " & strFileName & _
                                                               " does not exist.")
            End If


        End Sub
        Public Sub NewSpecDrawing()
            'define template to use
            'Updated template name 1/26/17 SCT
            Dim strTemplatePath As String = "C:\ProgramData\Autodesk\AutoCAD Templates\E-Series SPR Template.dwt"
            Dim acDocMgr As DocumentCollection = Application.DocumentManager
            Dim acDoc As Document = DocumentCollectionExtension.Add(acDocMgr, strTemplatePath)
            acDocMgr.MdiActiveDocument = acDoc
        End Sub
        Public Sub CloseDrawing()
            Dim obj As Object = Application.GetSystemVariable("DBMOD")
            Dim obj2 As Object = Application.GetSystemVariable("WRITESTAT")
            If Not (System.Convert.ToInt16(obj2) = 0) Then

                'Dim myDB As Database = HostApplicationServices.WorkingDatabase
                'Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                '    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                '    For Each myBTRid As ObjectId In myBlockTable
                '        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                '        If myBlockTableRecord.IsLayout = True Then
                '            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                '            Debug.IndentLevel = 1
                '            'MsgBox(myLayout.LayoutName)
                '            If myLayout.LayoutName = "Signoff P" Then
                '                switchlayout("Signoff P")
                '            ElseIf myLayout.LayoutName = "Elevation" Then
                '                switchlayout("Elevation")
                '            End If
                '        End If
                '    Next
                'End Using
                'ZoomExtents()
                '' Check the value of DBMOD, if 0 then the drawing has not been changed
                If Not (System.Convert.ToInt16(obj) = 0) Then
                    If MsgBox("Do you wish to save this drawing?", _
                              MsgBoxStyle.YesNo, _
                              "Save Drawing") = MsgBoxResult.Yes Then
                        Dim acDoc As Document = Application.DocumentManager.MdiActiveDocument
                        acDoc.Database.SaveAs(acDoc.Name, True, DwgVersion.Current, _
                                              acDoc.Database.SecurityParameters)
                    End If
                End If
                Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
                cdoc.SendStringToExecute("._close ", True, False, False)
            Else
                Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
                cdoc.SendStringToExecute("._close ", True, False, False)
            End If


        End Sub
        'Public Sub ReadElevationTitleBlocks()
        '    Dim doc As Document = Application.DocumentManager.MdiActiveDocument
        '    Using doc.LockDocument()
        '        Dim myDB As Database = HostApplicationServices.WorkingDatabase
        '        Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
        '            Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
        '            For Each myBTRid As ObjectId In myBlockTable
        '                Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
        '                If myBlockTableRecord.IsLayout = True Then
        '                    Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
        '                    Debug.IndentLevel = 1
        '                    'MsgBox(myLayout.LayoutName)
        '                    If myLayout.LayoutName = "Elevation" Then
        '                        Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
        '                        For Each entID As ObjectId In myBlockTableRecord
        '                            Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
        '                            If ent IsNot Nothing Then
        '                                Dim br As BlockReference = TryCast(ent, BlockReference)
        '                                If br IsNot Nothing Then
        '                                    Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
        '                                    If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
        '                                        For Each attReferenceId As ObjectId In br.AttributeCollection
        '                                            Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
        '                                            Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
        '                                            If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
        '                                                If attReference.Tag.ToUpper() = "SALEFORCE" Then     ' If so, update the value and increment the counter
        '                                                    strOTTNumber = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "SCALE" Then     ' If so, update the value and increment the counter
        '                                                    strDrawingScale = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "JOBNAME" Then     ' If so, update the value and increment the counter
        '                                                    strJobTag = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "CUSTOMER" Then     ' If so, update the value and increment the counter
        '                                                    strCustomerName = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "DWG_N" Then     ' If so, update the value and increment the counter
        '                                                    strDrawingNumber = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "REV" Then     ' If so, update the value and increment the counter
        '                                                    strRevNumber = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "QUOTE" Then     ' If so, update the value and increment the counter
        '                                                    strQuoteNumber = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "CUSTPO" Then     ' If so, update the value and increment the counter
        '                                                    strPurchaseOrder = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "PAGE" Then     ' If so, update the value and increment the counter
        '                                                    strPageNumber = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "DRAFTER" Then     ' If so, update the value and increment the counter
        '                                                    strUserName = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "CREATEDATE" Then     ' If so, update the value and increment the counter
        '                                                    strDWGDate = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "DEPARTMENT" Then     ' If so, update the value and increment the counter
        '                                                    strDrawingType = attReference.TextString
        '                                                ElseIf attReference.Tag.ToUpper() = "LAYOUTTAB" Then     ' If so, update the value and increment the counter
        '                                                    strMyLayoutName = attReference.TextString
        '                                                End If
        '                                            End If
        '                                        Next
        '                                    End If
        '                                    'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
        '                                End If
        '                            End If


        '                        Next
        '                    End If





        '                    ' MsgBox(myLayout.CanonicalMediaName)

        '                End If
        '                Debug.IndentLevel = 0
        '            Next
        '            myTrans.Commit()
        '        End Using
        '    End Using


        'End Sub
        Public Sub ReadSignoffTitleBlock()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName = "Signoff P" Then
                                LayoutFlag = False
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "Signoff" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "OTT_NUMBER" Then     ' If so, update the value and increment the counter
                                                            strOTTNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DRAWING_NUM" Then     ' If so, update the value and increment the counter
                                                            strDrawingNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "REVISION" Then     ' If so, update the value and increment the counter
                                                            strRevNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "PAGE_NUMBER" Then     ' If so, update the value and increment the counter
                                                            strPageNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DRAWN_BY" Then     ' If so, update the value and increment the counter
                                                            strUserName = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DATE_DRAWN" Then     ' If so, update the value and increment the counter
                                                            strDWGDate = attReference.TextString
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If
                                Next
                                'once we are migrate over we can elimenate the code for Elevation
                            ElseIf myLayout.LayoutName = "Elevation" Then
                                LayoutFlag = True
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "SALEFORCE" Then     ' If so, update the value and increment the counter
                                                            strOTTNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "SCALE" Then     ' If so, update the value and increment the counter
                                                            strDrawingScale = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "JOBNAME" Then     ' If so, update the value and increment the counter
                                                            strJobTag = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "CUSTOMER" Then     ' If so, update the value and increment the counter
                                                            strCustomerName = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DWG_N" Then     ' If so, update the value and increment the counter
                                                            strDrawingNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "REV" Then     ' If so, update the value and increment the counter
                                                            strRevNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "QUOTE" Then     ' If so, update the value and increment the counter
                                                            strQuoteNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "CUSTPO" Then     ' If so, update the value and increment the counter
                                                            strPurchaseOrder = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "PAGE" Then     ' If so, update the value and increment the counter
                                                            strPageNumber = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DRAFTER" Then     ' If so, update the value and increment the counter
                                                            strUserName = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "CREATEDATE" Then     ' If so, update the value and increment the counter
                                                            strDWGDate = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "DEPARTMENT" Then     ' If so, update the value and increment the counter
                                                            strDrawingType = attReference.TextString
                                                        ElseIf attReference.Tag.ToUpper() = "LAYOUTTAB" Then     ' If so, update the value and increment the counter
                                                            strMyLayoutName = attReference.TextString
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If
                                Next
                            End If
                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using


        End Sub
        Public Sub UpdateSupplierTitleBlocks() 'code validated 3/8/17
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim ApprovedDrawing As String = Path.GetFileNameWithoutExtension(doc.Name)
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName <> "Signoff P" Or myLayout.LayoutName <> "Signoff L" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "Supplier L" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "DRAWN_BY" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = GetDrafter()
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "DATE_DRAWN" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = GetDate()
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "DRAWING_NUM" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = ApprovedDrawing
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "REVISION" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = MfgRev
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "REV_TITLE" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = RevTag
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "REV_DATE" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = GetDate()
                                                            attReference.DowngradeOpen()
                                                        ElseIf attReference.Tag.ToUpper() = "REV_BY" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = GetDrafter()
                                                            attReference.DowngradeOpen()

                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If
                            ' MsgBox(myLayout.CanonicalMediaName)
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
        End Sub
        Public Sub UpdateLayoutTabInTitle()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            ' MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName <> "a Signoff L" Or myLayout.LayoutName <> "a Signoff P" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "Supplier L" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "TITLE" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = myLayout.LayoutName
                                                            attReference.DowngradeOpen()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If





                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using


        End Sub
        Public Sub ZoomExtents()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            ' switchlayout(myLayout.LayoutName)
                            Dim acadApp As Object = Application.AcadApplication
                            acadApp.ZoomExtents()
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using


        End Sub
        Public Sub switchlayout(ByVal LayoutToChange As String)
            Dim mydoc As Document = Application.DocumentManager.MdiActiveDocument
            Using mydoc.LockDocument()
                Dim acLayoutMgr As LayoutManager
                acLayoutMgr = LayoutManager.Current
                acLayoutMgr.CurrentLayout = LayoutToChange


            End Using
        End Sub
        Public Sub UpdateSignoffTitleBlocks()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                            For Each entID As ObjectId In myBlockTableRecord
                                Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                If ent IsNot Nothing Then
                                    Dim br As BlockReference = TryCast(ent, BlockReference)
                                    If br IsNot Nothing Then
                                        Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                        If bd.Name = "Signoff" Or bd.Name = "Signoff L" Then               ' Check each of the attributes...
                                            For Each attReferenceId As ObjectId In br.AttributeCollection
                                                Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                If attReference IsNot Nothing Then
                                                    'MsgBox(attReference.Tag) 
                                                    If attReference.Tag.ToUpper() = "OTT_NUMBER" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strOTTNumber
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DRAWING_NUM" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strDrawingNumber
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "REVISION" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strRevNumber
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "PAGE_NUMBER" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strPageNumber
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DRAWN_BY" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strUserName
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DATE_DRAWN" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strDWGDate
                                                        attReference.DowngradeOpen()
                                                        'ElseIf attReference.Tag.ToUpper() = "Grille_Type" Then     ' If so, update the value and increment the counter
                                                        '    attReference.UpgradeOpen()
                                                        '    attReference.TextString = "Stan"
                                                        '    attReference.DowngradeOpen()
                                                    End If
                                                End If
                                            Next
                                        End If
                                        'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                    End If
                                End If
                            Next
                            ' MsgBox(myLayout.CanonicalMediaName)
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
        End Sub
        Public Sub SignoffTitleBlockRevision()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                            For Each entID As ObjectId In myBlockTableRecord
                                Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                If ent IsNot Nothing Then
                                    Dim br As BlockReference = TryCast(ent, BlockReference)
                                    If br IsNot Nothing Then
                                        Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                        If bd.Name = "Signoff" Or bd.Name = "Signoff L" Then               ' Check each of the attributes...
                                            For Each attReferenceId As ObjectId In br.AttributeCollection
                                                Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                If attReference IsNot Nothing Then
                                                    'MsgBox(attReference.Tag) 
                                                    If attReference.Tag.ToUpper() = "OTT_NUMBER" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strOTTNumber
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "REVISION" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = revlevel
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DRAWN_BY" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strUserName
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DATE_DRAWN" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = strDWGDate
                                                        attReference.DowngradeOpen()
                                                    End If
                                                End If
                                            Next
                                        End If
                                        'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                    End If
                                End If
                            Next
                            ' MsgBox(myLayout.CanonicalMediaName)
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
        End Sub
        'Public Sub ChangeAllTitleBlocks()
        '    Dim doc As Document = Application.DocumentManager.MdiActiveDocument
        '    Using doc.LockDocument()
        '        Dim myDB As Database = HostApplicationServices.WorkingDatabase
        '        Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
        '            Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
        '            For Each myBTRid As ObjectId In myBlockTable
        '                Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
        '                If myBlockTableRecord.IsLayout = True Then
        '                    Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
        '                    Debug.IndentLevel = 1
        '                    ' MsgBox(myLayout.LayoutName)
        '                    Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
        '                    For Each entID As ObjectId In myBlockTableRecord
        '                        Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
        '                        If ent IsNot Nothing Then
        '                            Dim br As BlockReference = TryCast(ent, BlockReference)
        '                            If br IsNot Nothing Then
        '                                Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
        '                                If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
        '                                    For Each attReferenceId As ObjectId In br.AttributeCollection
        '                                        Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
        '                                        Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
        '                                        If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
        '                                            If attReference.Tag.ToUpper() = "SALEFORCE" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strOTTNumber
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "SCALE" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strDrawingScale
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "JOBNAME" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strJobTag
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "CUSTOMER" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strCustomerName
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "DWG_N" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strDrawingNumber
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "REV" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strRevNumber
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "QUOTE" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strQuoteNumber
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "CUSTPO" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strPurchaseOrder
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "PAGE" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strPageNumber
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "DRAFTER" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strUserName
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "CREATEDATE" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strDWGDate
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "DEPARTMENT" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strDrawingType
        '                                                attReference.DowngradeOpen()
        '                                            ElseIf attReference.Tag.ToUpper() = "LAYOUTTAB" Then     ' If so, update the value and increment the counter
        '                                                attReference.UpgradeOpen()
        '                                                attReference.TextString = strMyLayoutName
        '                                                attReference.DowngradeOpen()
        '                                            End If
        '                                        End If
        '                                    Next
        '                                End If
        '                                'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
        '                            End If
        '                        End If


        '                    Next




        '                    ' MsgBox(myLayout.CanonicalMediaName)

        '                End If
        '                Debug.IndentLevel = 0
        '            Next
        '            myTrans.Commit()
        '        End Using
        '    End Using


        'End Sub
        Public Sub ApprovedDrawing1()
            Dim myGetDrawingForm As New frmGetDrawingNum
            Select Case Application.ShowModalDialog(myGetDrawingForm)
                Case System.Windows.Forms.DialogResult.OK
                    Dim AppDrawingNumber As String = myGetDrawingForm.txtGetDrawing.Text
                    If NonApprovedFileExist(AppDrawingNumber) Then
                        ApprovedPathExist(AppDrawingNumber)
                        System.IO.File.Move(pathString & "Specials Drawings\NonApproved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg", pathString & "Specials Drawings\Approved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg")
                        OpenDrawing(pathString & "Specials Drawings\Approved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg")
                        MfgRev = "1"
                        RevTag = "Release To Production"
                        'UpdateSupplierTitleBlocks()
                        'UpdateGrilleTabNames()
                        switchlayout("Glass")
                    ElseIf ApprovedFileExist(AppDrawingNumber) Then
                        MsgBox("Drawing " & AppDrawingNumber & " has already been approved. Please validate if any changes have been made/")
                        Exit Sub
                    End If
                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub
            End Select
        End Sub
        Public Sub WriteDrawingApp()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim ApprovedDrawing As String = Path.GetFileNameWithoutExtension(doc.Name)
            Dim objFileDrawingInfo As Boolean
            'check to see if dirctory exist, if not create it
            Try
                If (System.IO.Directory.Exists(SprSupportPathString)) Then
                    'do nothing
                Else
                    System.IO.Directory.CreateDirectory(SprSupportPathString)
                End If
                'check to see if drawing information file exist, if not create it
                objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\AppDrawingInfo.txt")
                If objFileDrawingInfo = True Then
                    'do nothing
                Else
                    'Create file if it is missing
                    System.IO.File.Create(SprSupportPathString & "\AppDrawingInfo.txt").Close()

                    'MsgBox("Drawing Info file was created")
                End If
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
            'Write input data from DrawingInfo form to text file for future use.
            Try
                Dim InfoTxtFile As New System.IO.StreamWriter(SprSupportPathString & "\AppDrawingInfo.txt", True) 'True at the end appends to existing file.
                InfoTxtFile.WriteLine(ApprovedDrawing)
                InfoTxtFile.Close()
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
        End Sub
        Public Sub ReadDrawingApp()
            'Updated sub 2/4/27 to accomodate new title blocks. 
            Dim objFileDrawingInfo As Boolean
            objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\AppDrawingInfo.txt")
            If objFileDrawingInfo = True Then
                'do nothing
            Else
                'drawing info file does not exist
                MsgBox("Drawing Info file Does not exist")
                Exit Sub
            End If
            'Read data from info file
            Dim InfoTxtFile As New System.IO.StreamReader(SprSupportPathString & "\AppDrawingInfo.txt")
            strDrawingNumber = InfoTxtFile.ReadLine()
            InfoTxtFile.Close()
        End Sub
        Public Sub ApprovedDrawing() 'Code validated 3/8/17 SCT
            MfgRev = "1"
            RevTag = "Release To Production"
            UpdateSupplierTitleBlocks()
            UpdateGrilleTabNames()
            WriteDrawingApp()
            CloseDrawing()
        End Sub
        Public Sub SetLayoutOrder()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim db As Database = doc.Database
            Dim ed As Editor = doc.Editor
            Dim layouts = New Dictionary(Of String, Layout)()
            Using tr As Transaction = doc.TransactionManager.StartTransaction
                Dim layoutDICT As DBDictionary = DirectCast(tr.GetObject(db.LayoutDictionaryId, OpenMode.ForRead), DBDictionary)
                ed.WriteMessage(vbLf & "Before Sort")
                Dim j As Long = 0
                For Each dictEntry As DBDictionaryEntry In layoutDICT
                    Dim layout As Layout = TryCast(tr.GetObject(dictEntry.Value, OpenMode.ForRead), Layout)
                    If Not layout.ModelType Then
                        layouts.Add(layout.LayoutName, layout)
                        ed.WriteMessage(vbLf & " Layout[{0}]={1}, tab={2}", System.Math.Max(System.Threading.Interlocked.Increment(j), j - 1), layout.LayoutName, layout.TabOrder)

                    End If
                Next
                Dim sortedLayoutNames = layouts.Keys.Where(Function(n) n.Contains("-"c)).[Select](Function(n) n.Split(New Char() {"-"c})).OrderBy(Function(n) n(0)).ThenBy(Function(n) n(1)).ThenBy(Function(n) Convert.ToInt32(n(2))).[Select](Function(n) String.Join("-", n)).[Select](Function(n, i)
                                                                                                                                                                                                                                                                                              layouts(n).UpgradeOpen()
                                                                                                                                                                                                                                                                                              layouts(n).TabOrder = i + 1
                                                                                                                                                                                                                                                                                              Return i + layouts(n).LayoutName

                                                                                                                                                                                                                                                                                          End Function).ToList()


                ed.WriteMessage(vbLf & "After Sort")
                j = 0
                For Each dictEntry As DBDictionaryEntry In layoutDICT
                    Dim layout As Layout = TryCast(tr.GetObject(dictEntry.Value, OpenMode.ForRead), Layout)
                    ed.WriteMessage(vbLf & " Layout[{0}]={1}, tab={2}", System.Math.Max(System.Threading.Interlocked.Increment(j), j - 1), layout.LayoutName, layout.TabOrder)
                Next

                tr.Commit()
            End Using

        End Sub



        Public Sub CopyLayout()
            Dim mydrawing As Autodesk.AutoCAD.ApplicationServices.Document = Application.DocumentManager.MdiActiveDocument
            Dim myDwg As Document = Application.DocumentManager.MdiActiveDocument
            Dim LayoutList As New ArrayList
            Dim tabOrder As Integer
            Dim newLayoutName As String = ""
            Using myTrans As Transaction = myDwg.TransactionManager.StartTransaction
                Dim myBT As BlockTable = myDwg.Database.BlockTableId.GetObject(OpenMode.ForRead)
                For Each myBtrID As ObjectId In myBT

                    Dim myBTR As BlockTableRecord = myBtrID.GetObject(OpenMode.ForRead)
                    If myBTR.IsLayout = True Then
                        Dim myLayout As Layout = myBTR.LayoutId.GetObject(OpenMode.ForRead)
                        Debug.IndentLevel = 1
                        tabOrder = myLayout.TabOrder
                        'MsgBox(myLayout.LayoutName & "-" & tabOrder)
                        If myLayout.LayoutName <> "Model" Then
                            LayoutList.Add(myLayout.LayoutName)
                        End If
                    End If
                    Debug.IndentLevel = 0
                Next
            End Using
            Dim GetLayoutToCopy As New frmPositionNumber
            LayoutList.Sort()
            GetLayoutToCopy.cbxLayoutTabs.DataSource = LayoutList
            Select Case Application.ShowModalDialog(GetLayoutToCopy)
                Case System.Windows.Forms.DialogResult.OK
                    Dim curLayout As String = GetLayoutToCopy.cbxLayoutTabs.Text
                    Dim posnumber As String = GetLayoutToCopy.cbxPositionNum.Text
                    Dim DLONumber As String = GetLayoutToCopy.cbxDLOPosition.Text
                    Dim myDrawingNumber As String = Application.GetSystemVariable("DWGNAME")
                    If Left(curLayout, 5) = "Glass" Then
                        newLayoutName = "Glass " & "-" & posnumber
                        CloneLayout(curLayout, newLayoutName)
                    ElseIf Left(curLayout, 7) = "Interio" Then
                        newLayoutName = "Interior Grille Placement " & "-" & posnumber & DLONumber
                        CloneLayout(curLayout, newLayoutName)
                    ElseIf Left(curLayout, 7) = "Exterio" Then
                        newLayoutName = "Exterior Grille Placement " & "-" & posnumber & DLONumber
                        CloneLayout(curLayout, newLayoutName)
                    ElseIf Left(curLayout, 5) = "Manuf" Then
                        newLayoutName = "Manufacturing " & "-" & posnumber
                        CloneLayout(curLayout, newLayoutName)
                    ElseIf Left(curLayout, 4) = "Part" Then
                        newLayoutName = "Parts " & "-" & posnumber
                        CloneLayout(curLayout, newLayoutName)
                    End If
                Case System.Windows.Forms.DialogResult.Cancel
                    'do nothing, caneled by user

            End Select
            'SortLayouts()
            'etLayoutOrder()

            switchlayout(newLayoutName)
            UpdateLayoutTabInTitle()
        End Sub
        Public Sub CloneLayout(ByVal CurLayout As String, ByVal NewLayout As String)
            '' Get the current document and database
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim acDoc As Document = Application.DocumentManager.MdiActiveDocument
            Dim acCurDb As Database = acDoc.Database

            Using doc.LockDocument()
                Using myTrans As Transaction = acCurDb.TransactionManager.StartTransaction
                    Dim acBlkTbl As BlockTable
                    acBlkTbl = myTrans.GetObject(acCurDb.BlockTableId, OpenMode.ForRead)
                    Dim myLM As LayoutManager = LayoutManager.Current
                    acBlkTbl.UpgradeOpen()
                    Dim TabCount As Integer = CurLayout.Count
                    myLM.CloneLayout(CurLayout, NewLayout, myLM.LayoutCount)
                    'myLM.CloneLayout(CurLayout, NewLayout, CurLayout.Count + 1)

                    acBlkTbl.DowngradeOpen()
                    myTrans.Commit()
                    Application.DocumentManager.MdiActiveDocument.Editor.Regen()
                End Using
            End Using
        End Sub
        Public Sub SortLayouts()
            Dim mydrawing As Autodesk.AutoCAD.ApplicationServices.Document = Application.DocumentManager.MdiActiveDocument
            Dim myDwg As Document = Application.DocumentManager.MdiActiveDocument
            Dim LayoutList As New ArrayList
            Dim tabOrder As Integer
            Using myTrans As Transaction = myDwg.TransactionManager.StartTransaction
                Dim myBT As BlockTable = myDwg.Database.BlockTableId.GetObject(OpenMode.ForRead)
                For Each myBtrID As ObjectId In myBT
                    Dim myBTR As BlockTableRecord = myBtrID.GetObject(OpenMode.ForRead)
                    If myBTR.IsLayout = True Then
                        Dim myLayout As Layout = myBTR.LayoutId.GetObject(OpenMode.ForRead)
                        Debug.IndentLevel = 1
                        tabOrder = myLayout.TabOrder
                        'MsgBox(myLayout.LayoutName & "-" & tabOrder)
                        If myLayout.LayoutName = "Model" Then
                            LayoutList.Insert(0, myLayout.LayoutName)
                        ElseIf myLayout.LayoutName = "Signoff P" Then
                            LayoutList.Insert(1, myLayout.LayoutName)
                        ElseIf myLayout.LayoutName = "Signoff L" Then
                            LayoutList.Insert(2, myLayout.LayoutName)
                        ElseIf myLayout.LayoutName = "Glass" Then
                            LayoutList.Insert(3, myLayout.LayoutName)

                        End If
                    End If
                    Debug.IndentLevel = 0
                Next
            End Using
            LayoutList.Sort()
            Dim tab As Integer = 1
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim db As Database = doc.Database
            Dim ed As Editor = doc.Editor
            Using doc.LockDocument()
                Using tr As Transaction = db.TransactionManager.StartTransaction()
                    For Each lay As String In LayoutList
                        Dim dLayouts As DBDictionary = tr.GetObject(db.LayoutDictionaryId, OpenMode.ForRead)
                        If (dLayouts.Contains(lay)) Then
                            Dim id As ObjectId = dLayouts.GetAt(lay)
                            Dim layout As Layout = tr.GetObject(id, OpenMode.ForRead)
                            layout.UpgradeOpen()
                            layout.TabOrder = tab
                            layout.DowngradeOpen()
                            tab = tab + 1
                        End If
                    Next
                    tr.Commit()
                    Application.DocumentManager.MdiActiveDocument.Editor.Regen()
                End Using
            End Using
        End Sub


        <CommandMethod("Test")> _
        Public Sub Test()

        End Sub
        <CommandMethod("Test2")> _
        Public Sub Test2()
        End Sub
        Public Sub UpdateGrilleTabNames()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName = "Interior Grille Placement" Then
                                myLayout.UpgradeOpen()
                                myLayout.LayoutName = "Interior Grille Placement-1D1"
                                myLayout.DowngradeOpen()
                            ElseIf myLayout.LayoutName = "Exterior Grille Placement" Then
                                myLayout.UpgradeOpen()
                                myLayout.LayoutName = "Exterior Grille Placement-1D1"
                                myLayout.DowngradeOpen()

                            End If
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
            'SortLayouts()
        End Sub
        Public Sub ReadDrawingInfo()
            'Updated sub 2/4/27 to accomodate new title blocks. 
            Dim objFileDrawingInfo As Boolean
            objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\DrawingInfo.txt")
            If objFileDrawingInfo = True Then
                'do nothing
            Else
                'drawing info file does not exist
                MsgBox("Drawing Info file Does not exist")
                Exit Sub
            End If
            'Read data from info file
            Dim InfoTxtFile As New System.IO.StreamReader(SprSupportPathString & "\DrawingInfo.txt")
            strDrawingNumber = InfoTxtFile.ReadLine()
            'strPageNumber = InfoTxtFile.ReadLine()
            strSubDirectory = InfoTxtFile.ReadLine()
            strOTTNumber = InfoTxtFile.ReadLine()
            strRevNumber = InfoTxtFile.ReadLine()
            InfoTxtFile.Close()
        End Sub

        Public Sub WriteDrawingInfo(ByVal SubDir As String)

            Dim objFileDrawingInfo As Boolean
            'check to see if dirctory exist, if not create it
            Try
                If (System.IO.Directory.Exists(SprSupportPathString)) Then
                    'do nothing
                Else
                    System.IO.Directory.CreateDirectory(SprSupportPathString)
                End If
                'check to see if drawing information file exist, if not create it
                objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\DrawingInfo.txt")
                If objFileDrawingInfo = True Then
                    'do nothing
                Else
                    'Create file if it is missing
                    System.IO.File.Create(SprSupportPathString & "\DrawingInfo.txt").Close()

                    'MsgBox("Drawing Info file was created")
                End If
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
            'Write input data from DrawingInfo form to text file for future use.
            Try
                Dim InfoTxtFile As New System.IO.StreamWriter(SprSupportPathString & "\DrawingInfo.txt")
                InfoTxtFile.WriteLine(Left(Application.GetSystemVariable("DWGNAME"), 6))
                'InfoTxtFile.WriteLine(strDrawingNumber)
                'InfoTxtFile.WriteLine(strPageNumber)
                InfoTxtFile.WriteLine(SubDir)
                InfoTxtFile.WriteLine(strOTTNumber)
                InfoTxtFile.Write(strRevNumber)
                InfoTxtFile.Close()
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
        End Sub

        Public Sub DeleteDrawingInfo(ByVal SubDir As String)

            Dim objFileDrawingInfo As Boolean
            'check to see if dirctory exist, if not create it
            Try
                If (System.IO.Directory.Exists(SprSupportPathString)) Then
                    'do nothing
                Else
                    System.IO.Directory.CreateDirectory(SprSupportPathString)
                End If
                'check to see if drawing information file exist, if not create it
                objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\DrawingInfo.txt")
                If objFileDrawingInfo = True Then
                    'do nothing
                Else
                    'Create file if it is missing
                    System.IO.File.Create(SprSupportPathString & "\DrawingInfo.txt").Close()

                    'MsgBox("Drawing Info file was created")
                End If
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
            'Write input data from DrawingInfo form to text file for future use.
            Try
                Dim InfoTxtFile As New System.IO.StreamWriter(SprSupportPathString & "\DrawingInfo.txt")
                InfoTxtFile.WriteLine("")
                InfoTxtFile.WriteLine("")
                InfoTxtFile.WriteLine("")
                InfoTxtFile.Write("")
                InfoTxtFile.Close()
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
        End Sub
        Public Function GetNewPage()
            Dim newPageNumber As String = ""
            Dim curPageNumber As String = Left(Mid(Application.GetSystemVariable("DWGNAME"), 7), 1)
            If curPageNumber = "A" Then
                newPageNumber = "B"
            ElseIf curPageNumber = "B" Then
                newPageNumber = "C"
            ElseIf curPageNumber = "C" Then
                newPageNumber = "D"
            ElseIf curPageNumber = "D" Then
                newPageNumber = "E"
            ElseIf curPageNumber = "E" Then
                newPageNumber = "F"
            ElseIf curPageNumber = "F" Then
                newPageNumber = "G"
            ElseIf curPageNumber = "G" Then
                newPageNumber = "H"
            ElseIf curPageNumber = "H" Then
                newPageNumber = "J"
            ElseIf curPageNumber = "J" Then
                newPageNumber = "K"
            ElseIf curPageNumber = "K" Then
                newPageNumber = "L"
            ElseIf curPageNumber = "L" Then
                newPageNumber = "M"
            ElseIf curPageNumber = "M" Then
                newPageNumber = "N"
            ElseIf curPageNumber = "N" Then
                newPageNumber = "P"
            ElseIf curPageNumber = "P" Then
                newPageNumber = "Q"
            ElseIf curPageNumber = "Q" Then
                newPageNumber = "R"
            ElseIf curPageNumber = "R" Then
                newPageNumber = "S"
            ElseIf curPageNumber = "S" Then
                newPageNumber = "T"
            ElseIf curPageNumber = "T" Then
                newPageNumber = "U"
            ElseIf curPageNumber = "U" Then
                newPageNumber = "V"
            ElseIf curPageNumber = "V" Then
                newPageNumber = "W"
            ElseIf curPageNumber = "W" Then
                newPageNumber = "X"
            ElseIf curPageNumber = "X" Then
                newPageNumber = "Y"
            ElseIf curPageNumber = "Y" Then
                MsgBox("You have reached the last available page for this drawing, please start a new drawing number")
                AddDrawingToJob = "No"
            End If

            Return newPageNumber
        End Function
        Public Sub SignoffRevision()

            'Dim myGetDrawingForm As New frmGetDrawingNum
            'Select Case Application.ShowModalDialog(myGetDrawingForm)
            '    Case System.Windows.Forms.DialogResult.OK
            '        Dim AppDrawingNumber As String = myGetDrawingForm.txtGetDrawing.Text
            '        If NonApprovedFileExist(AppDrawingNumber) Then
            '            ApprovedPathExist(AppDrawingNumber)
            '            'System.IO.File.Move(pathString & "Specials Drawings\NonApproved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg", pathString & "Specials Drawings\Approved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg")
            '            OpenDrawing(pathString & "Specials Drawings\NonApproved\" & (getSubDir(Left(AppDrawingNumber, 6))) & "\" & AppDrawingNumber & ".dwg")
            '            'UpdateSupplierTitleBlocks()
            '            'UpdateGrilleTabNames()
            '            switchlayout("a Signoff P")
            '        ElseIf ApprovedFileExist(AppDrawingNumber) Then
            '            MsgBox("Drawing " & AppDrawingNumber & " has already been approved. No changes can be made, a new drawing is required")
            '            Exit Sub
            '        End If
            '    Case System.Windows.Forms.DialogResult.Cancel
            '        Exit Sub
            'End Select
            Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
            Dim CurRev As String = GetCurrentSignoffRevision()
            ReadSignoffTitleBlock()
            If CurRev = "1" Then
                revlevel = "2"
                MfgRev = "P-2"
            ElseIf CurRev = "2" Then
                revlevel = "3"
                MfgRev = "P-3"
            ElseIf CurRev = "3" Then
                revlevel = "4"
                MfgRev = "P-4"
            ElseIf CurRev = "4" Then
                revlevel = "5"
                MfgRev = "P-5"
            ElseIf CurRev = "5" Then
                revlevel = "6"
                MfgRev = "P-6"
            ElseIf CurRev = "6" Then
                revlevel = "7"
                MfgRev = "P-7"
            ElseIf CurRev = "7" Then
                revlevel = "8"
                MfgRev = "P-8"
            ElseIf CurRev = "8" Then
                revlevel = "9"
                MfgRev = "P-9"
            ElseIf CurRev = "9" Then
                revlevel = "10"
                MfgRev = "P-10"
            ElseIf CurRev = "10" Then
                revlevel = "11"
                MfgRev = "P-11"
            End If
            Dim myOTTNumber As New frmOTTNUmber
            myOTTNumber.txtOTTNumber.Text = strOTTNumber
            'Open Drawing Information dialog 
            Select Case Application.ShowModalDialog(myOTTNumber)
                Case System.Windows.Forms.DialogResult.OK
                    'Set public variables              
                    strOTTNumber = myOTTNumber.txtOTTNumber.Text
                    strDWGDate = GetDate()
                    strUserName = GetDrafter()

                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub
            End Select
            RevTag = "Pre-Production"
            SignoffTitleBlockRevision()
            UpdateSupplierTitleBlocks()

        End Sub





        Public Sub InsertRevision()
            Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
            Dim CurRev As String = GetElevationCurrentRevision()
            Dim revBlock As String = ""
            Dim insp As String = ".0625,1.441,0"

            revdrafter = GetDrafter()
            revdate = GetDate()
            Dim myRevisionForm As New frmRevisionInfo

            If CurRev = "A" Then
                revBlock = "RevB"
                revlevel = "B"
                insp = ".0625,1.441,0"
            ElseIf CurRev = "B" Then
                revBlock = "RevC"
                revlevel = "C"
                insp = ".0625,1.629,0"
            ElseIf CurRev = "C" Then
                revBlock = "RevD"
                revlevel = "D"
                insp = ".0625,1.817,0"
            ElseIf CurRev = "D" Then
                revBlock = "RevE"
                revlevel = "E"
                insp = ".0625,2.005,0"
            ElseIf CurRev = "E" Then
                revBlock = "RevF"
                revlevel = "F"
                insp = ".0625,2.193,0"
            ElseIf CurRev = "F" Then
                revBlock = "RevG"
                revlevel = "G"
                insp = ".0625,2.381,0"
            ElseIf CurRev = "G" Then
                revBlock = "RevH"
                revlevel = "H"
                insp = ".0625,2.569,0"
            ElseIf CurRev = "H" Then
                revBlock = "RevJ"
                revlevel = "J"
                insp = ".0625,2.757,0"
            ElseIf CurRev = "J" Then
                revBlock = "RevK"
                revlevel = "K"
                insp = ".0625,2.945,0"
            ElseIf CurRev = "K" Then
                revBlock = "RevL"
                revlevel = "L"
                insp = ".0625,3.133,0"


            End If

            'strDrawingNumber = mydrawing.Name()
            '' ReadRevisionInfo()

            'If Left(revDrawingNum, 7) = Left(strDrawingNumber, 7) Then

            'Else
            '    revsalesforce = ""
            '    revdescription = ""
            'End If

            myRevisionForm.txtRevisionLevel.Text = revlevel
            myRevisionForm.txtDrafter.Text = revdrafter
            myRevisionForm.txtRevisionDate.Text = revdate



            Select Case Application.ShowModalDialog(myRevisionForm)
                Case System.Windows.Forms.DialogResult.OK
                    revlevel = myRevisionForm.txtRevisionLevel.Text
                    revdrafter = myRevisionForm.txtDrafter.Text
                    revdate = myRevisionForm.txtRevisionDate.Text
                    RevTag = myRevisionForm.txrRevDes.Text
                    revsalesforce = myRevisionForm.txtSalesforce.Text
                    'Dim mystring As String = "_-insert _" & revBlock & " " & insp & " 1 1 0 _" & revlevel & " _" & revsalesforce & " _" & revdescription & " _" & revdrafter & " _" & revdate & " "
                    ' cdoc.SendStringToExecute(mystring, True, False, False)
                    'cdoc.SendStringToExecute("-insert RevB .0625,1.441,0 1 1 0", True, False, False)

                    Dim app As AcadApplication = TryCast(Application.AcadApplication, AcadApplication)
                    Application.SetSystemVariable("ATTDIA", 0)
                    app.ActiveDocument.SendCommand("-insert" & vbCr & revBlock & vbCr & insp & vbCr & "1" & vbCr & "1" & vbCr & "0" & vbCr & revlevel & vbCr & revsalesforce & vbCr & RevTag & vbCr & revdrafter & vbCr & revdate & vbCr)
                    Application.SetSystemVariable("ATTDIA", 1)

                Case System.Windows.Forms.DialogResult.Cancel


            End Select
            SetElevationRevision(revlevel)

        End Sub
        Public Function GetElevationCurrentRevision()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim CurrentRevision As String = ""
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName = "Elevation" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "REV" Then
                                                            CurrentRevision = attReference.TextString
                                                            'attReference.UpgradeOpen()
                                                            'attReference.TextString = "" 'ScaleString
                                                            'attReference.DowngradeOpen()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If





                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
            Return CurrentRevision

        End Function
        Public Function GetCurrentSignoffRevision()
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim CurrentRevision As String = ""
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName = "Signoff P" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "Signoff" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "REVISION" Then
                                                            CurrentRevision = attReference.TextString
                                                            'attReference.UpgradeOpen()
                                                            'attReference.TextString = "" 'ScaleString
                                                            'attReference.DowngradeOpen()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If





                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
            Return CurrentRevision

        End Function

        Public Sub SetElevationRevision(ByVal RevString As String)
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName = "Elevation" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "REV" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = RevString
                                                            attReference.DowngradeOpen()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If





                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using


        End Sub
        Public Sub SetApprovalRevision(ByVal RevString As String)
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            If myLayout.LayoutName <> "Elevation" Then
                                Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                                For Each entID As ObjectId In myBlockTableRecord
                                    Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                    If ent IsNot Nothing Then
                                        Dim br As BlockReference = TryCast(ent, BlockReference)
                                        If br IsNot Nothing Then
                                            Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                            If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
                                                For Each attReferenceId As ObjectId In br.AttributeCollection
                                                    Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                    Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                    If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                        If attReference.Tag.ToUpper() = "REV" Then     ' If so, update the value and increment the counter
                                                            attReference.UpgradeOpen()
                                                            attReference.TextString = RevString
                                                            attReference.DowngradeOpen()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                        End If
                                    End If


                                Next
                            End If





                            ' MsgBox(myLayout.CanonicalMediaName)

                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using


        End Sub


        Public Sub MoveApprovedFile()
            Dim SourceFile As String = "AppDrawingInfo.txt"
            'check to see if file that contains the list of approved  drawing exist.
            Dim objFileDrawingInfo As Boolean
            objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\AppDrawingInfo.txt")
            If objFileDrawingInfo = True Then
                'do nothing
            Else
                'drawing info file does not exist
                MsgBox("Drawing list of approved files does not exist.")
                Exit Sub
            End If
            Dim text As String = System.IO.File.ReadAllText(SprSupportPathString & "\AppDrawingInfo.txt")
            'file is empty!
            If text.Length <> 0 Then
                Dim sr As New StreamReader(SprSupportPathString & "\AppDrawingInfo.txt")
                Dim ApprovedDrawingNumber As String
                Dim NumberOfLines As Integer
                Do While sr.Peek >= 0
                    ApprovedDrawingNumber = sr.ReadLine()
                    If NonApprovedFileExist(ApprovedDrawingNumber) Then
                        ApprovedPathExist(ApprovedDrawingNumber)
                        System.IO.File.Move(pathString & "Specials Drawings\NonApproved\" & (getSubDir(Left(ApprovedDrawingNumber, 6))) & "\" & ApprovedDrawingNumber & ".dwg", pathString & "Specials Drawings\Approved\" & (getSubDir(Left(ApprovedDrawingNumber, 6))) & "\" & ApprovedDrawingNumber & ".dwg")
                        NumberOfLines += 1
                    Else
                        MsgBox("Drawing " & ApprovedDrawingNumber & " was not found.")
                    End If

                Loop
                MsgBox(NumberOfLines & " drawings where moved to the approved folder.")
                sr.Close()
                'Dim InfoTxtFile As New System.IO.StreamWriter(SprSupportPathString & "\AppDrawingInfo.txt", False)
                File.WriteAllText(SprSupportPathString & "\AppDrawingInfo.txt", "")
                'InfoTxtFile.Close()

            Else
                MsgBox("There are no approved drawings to move.")

            End If

           

        End Sub
        Public Sub IQApproval()

            Dim myGetDrawingForm As New frmGetDrawingNum
            Select Case Application.ShowModalDialog(myGetDrawingForm)
                Case System.Windows.Forms.DialogResult.OK
                    Dim IQDrawingNumber As String = myGetDrawingForm.txtGetDrawing.Text
                    'MsgBox(getSubDir(IQDrawingNumber) & " / " & IQDrawingNumber)
                    Dim FilePath As String = pathString & "Specials Drawings\Approved\" & getSubDir(IQDrawingNumber)
                    'Check to see if sub dirrectory folder exists on network
                    Dim folderexist As Boolean
                    folderexist = System.IO.Directory.Exists(FilePath)
                    If folderexist = True Then
                        'Sub directory already exist, do nothing
                    Else
                        'Create Specials drawing sub folder if is does not already exist.
                        System.IO.Directory.CreateDirectory(FilePath)
                    End If
                    'create file path
                    Dim NewFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(IQDrawingNumber) & "\" & IQDrawingNumber & ".dwg"
                    'Check to see if sub dirrectory folder exists on network
                    Dim FileExist As Boolean
                    FileExist = System.IO.File.Exists(NewFile)
                    If FileExist = True Then
                        MsgBox("Drawing " & IQDrawingNumber & " already exist, check your drawing number.")
                        Exit Sub
                    Else
                        'Open new IQ Approval drawing template
                        Dim strTemplatePath As String = "C:\ProgramData\Autodesk\AutoCAD Templates\IQ Approval Template.dwt"
                        Dim acDocMgr As DocumentCollection = Application.DocumentManager
                        Dim acDoc As Document = DocumentCollectionExtension.Add(acDocMgr, strTemplatePath)
                        acDocMgr.MdiActiveDocument = acDoc
                        'Save drawing
                        'Dim newFileName As String = pathString & "Specials Drawings\NonApproved\" & (getSubDir(newDrawingNumber)) & "\" & newDrawingNumber & newPageNumber & ".dwg"
                        SaveActiveDrawing(NewFile)
                        UpdateIQApprovalTitleBlocks(IQDrawingNumber)
                        WriteLastDrawingAprroved(IQDrawingNumber)
                    End If
                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub
            End Select
            'MsgBox("You made it.")
        End Sub
        Public Sub IQOpenDrawing()
            Dim myGetDrawingForm As New frmGetDrawingNum
            Select Case Application.ShowModalDialog(myGetDrawingForm)
                Case System.Windows.Forms.DialogResult.OK
                    Dim IQDrawingNumber As String = myGetDrawingForm.txtGetDrawing.Text
                    'MsgBox(getSubDir(IQDrawingNumber) & " / " & IQDrawingNumber)
                    'create file path to open
                    Dim NewFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(IQDrawingNumber) & "\" & IQDrawingNumber & ".dwg"
                    'Check to see if sub dirrectory folder exists on network
                    Dim acDocMgr As DocumentCollection = Application.DocumentManager
                    Dim FileExist As Boolean
                    FileExist = System.IO.File.Exists(NewFile)
                    If FileExist = True Then
                        If IQApprovedLockFileExist(IQDrawingNumber) = True Then
                            'Determine user of lock file
                            Dim LockFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(IQDrawingNumber) & "\" & IQDrawingNumber & ".dwl"
                            Dim fs As FileSecurity = File.GetAccessControl(LockFile)
                            Dim sid As IdentityReference = fs.GetOwner(GetType(SecurityIdentifier))
                            Dim ntaccount As IdentityReference = sid.Translate(GetType(NTAccount))
                            Dim owner As String = ntaccount.ToString()
                            MsgBox("File is already open by " & owner & ".")
                        Else
                            'No lock file found so open drawing read only.
                            DocumentCollectionExtension.Open(acDocMgr, NewFile, True)
                        End If
                    Else
                        MsgBox("Drawing " & NewFile & " does not exist.")

                    End If
                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub
            End Select
        End Sub

        Public Sub IQCopyDrawing()
            Dim myGetDrawingForm As New frmIQCopy
            Dim myDrawNumber As String = ReadLastDrawingAprroved()
            myGetDrawingForm.txtCopyDrawing.Text = myDrawNumber
            myGetDrawingForm.txtNewDrawing.Text = myDrawNumber
            Select Case Application.ShowModalDialog(myGetDrawingForm)
                Case System.Windows.Forms.DialogResult.OK
                    Dim CopyDrawingNumber As String = myGetDrawingForm.txtCopyDrawing.Text
                    Dim NewDrawingNumber As String = myGetDrawingForm.txtNewDrawing.Text
                    If NewDrawingNumber <> CopyDrawingNumber Then
                        'create file path to open
                        Dim CopyFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(CopyDrawingNumber) & "\" & CopyDrawingNumber & ".dwg"
                        Dim NewFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(NewDrawingNumber) & "\" & NewDrawingNumber & ".dwg"
                        'Check to see if sub dirrectory folder exists on network
                        Dim acDocMgr As DocumentCollection = Application.DocumentManager
                        Dim FileExist As Boolean
                        FileExist = System.IO.File.Exists(CopyFile)
                        If FileExist = True Then

                            If IQApprovedLockFileExist(CopyDrawingNumber) = True Then
                                'Determine user of lock file
                                Dim LockFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(CopyDrawingNumber) & "\" & CopyDrawingNumber & ".dwl"
                                Dim fs As FileSecurity = File.GetAccessControl(LockFile)
                                Dim sid As IdentityReference = fs.GetOwner(GetType(SecurityIdentifier))
                                Dim ntaccount As IdentityReference = sid.Translate(GetType(NTAccount))
                                Dim owner As String = ntaccount.ToString()
                                MsgBox("File is already open by " & owner & ".")
                            Else
                                'No lock file found so copy file and open drawing for edit.
                                IQApprovedPathExist(NewDrawingNumber)
                                System.IO.File.Copy(CopyFile, NewFile)
                                DocumentCollectionExtension.Open(acDocMgr, NewFile, False)
                                UpdateIQApprovalTitleBlocks(NewDrawingNumber)
                            End If
                        Else
                            MsgBox("Drawing " & NewFile & " does not exist to copy.")
                        End If
                    Else
                        MsgBox("Copy and New drawing numbers match, please try again")
                    End If
                Case System.Windows.Forms.DialogResult.Cancel
                    Exit Sub


            End Select
            myGetDrawingForm.txtCopyDrawing.Text = ""
            myGetDrawingForm.txtNewDrawing.Text = ""
        End Sub

        Public Function IQApprovedLockFileExist(ByVal DrawingNumber As String)
            Dim NonAppFile As String = pathString & "Specials Drawings\Approved\" & getSubDir(DrawingNumber) & "\" & DrawingNumber & ".dwl"

            'Check to see if sub dirrectory folder exists on network
            Dim File As Boolean
            File = System.IO.File.Exists(NonAppFile)
            Return File
        End Function


        Public Sub UpdateIQApprovalTitleBlocks(ByVal MyIQDrawingNumber As String) 'alidated 3/8/17
            Dim doc As Document = Application.DocumentManager.MdiActiveDocument
            Dim ApprovedDrawing As String = Path.GetFileNameWithoutExtension(doc.Name)
            Using doc.LockDocument()
                Dim myDB As Database = HostApplicationServices.WorkingDatabase
                Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                    Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                    For Each myBTRid As ObjectId In myBlockTable
                        Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                        If myBlockTableRecord.IsLayout = True Then
                            Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                            Debug.IndentLevel = 1
                            'MsgBox(myLayout.LayoutName)
                            ' If myLayout.LayoutName <> "Signoff P" Or myLayout.LayoutName <> "Signoff L" Then
                            Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                            For Each entID As ObjectId In myBlockTableRecord
                                Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                If ent IsNot Nothing Then
                                    Dim br As BlockReference = TryCast(ent, BlockReference)
                                    If br IsNot Nothing Then
                                        Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                        If bd.Name = "Supplier L" Then               ' Check each of the attributes...
                                            For Each attReferenceId As ObjectId In br.AttributeCollection
                                                Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                    If attReference.Tag.ToUpper() = "DRAWN_BY" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = GetDrafter()
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DATE_DRAWN" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = GetDate()
                                                        attReference.DowngradeOpen()
                                                    ElseIf attReference.Tag.ToUpper() = "DRAWING_NUM" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = MyIQDrawingNumber
                                                        attReference.DowngradeOpen()
                                                        'ElseIf attReference.Tag.ToUpper() = "REVISION" Then     ' If so, update the value and increment the counter
                                                        '    attReference.UpgradeOpen()
                                                        '    attReference.TextString = MfgRev
                                                        '    attReference.DowngradeOpen()
                                                        'ElseIf attReference.Tag.ToUpper() = "REV_TITLE" Then     ' If so, update the value and increment the counter
                                                        '    attReference.UpgradeOpen()
                                                        '    attReference.TextString = RevTag
                                                        '    attReference.DowngradeOpen()
                                                        'ElseIf attReference.Tag.ToUpper() = "REV_DATE" Then     ' If so, update the value and increment the counter
                                                        '    attReference.UpgradeOpen()
                                                        '    attReference.TextString = GetDate()
                                                        '    attReference.DowngradeOpen()
                                                        'ElseIf attReference.Tag.ToUpper() = "REV_BY" Then     ' If so, update the value and increment the counter
                                                        '    attReference.UpgradeOpen()
                                                        '    attReference.TextString = GetDrafter()
                                                        '    attReference.DowngradeOpen()

                                                    End If
                                                End If
                                            Next
                                        End If
                                        'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                    End If
                                End If


                            Next
                            'nd If
                            ' MsgBox(myLayout.CanonicalMediaName)
                        End If
                        Debug.IndentLevel = 0
                    Next
                    myTrans.Commit()
                End Using
            End Using
        End Sub
        Public Sub WriteLastDrawingAprroved(ByVal MyDrawingNumber As String)

            Dim objFileDrawingInfo As Boolean
            'check to see if dirctory exist, if not create it
            Try
                If (System.IO.Directory.Exists(SprSupportPathString)) Then
                    'do nothing
                Else
                    System.IO.Directory.CreateDirectory(SprSupportPathString)
                End If
                'check to see if drawing information file exist, if not create it
                objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\LastApproval.txt")
                If objFileDrawingInfo = True Then
                    'do nothing
                Else
                    'Create file if it is missing
                    System.IO.File.Create(SprSupportPathString & "\LastApproval.txt").Close()
                End If
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
            'Write input data from DrawingInfo form to text file for future use.
            Try
                Dim InfoTxtFile As New System.IO.StreamWriter(SprSupportPathString & "\LastApproval.txt")
                InfoTxtFile.WriteLine(MyDrawingNumber)
                InfoTxtFile.Close()
            Catch ex As Autodesk.AutoCAD.Runtime.Exception
                Application.ShowAlertDialog("The following exception was caught:" & _
                                         vbLf & ex.Message)
            End Try
        End Sub
        Public Function ReadLastDrawingAprroved()
            'Updated sub 2/4/27 to accomodate new title blocks. 
            Dim mydraw As String
            Dim objFileDrawingInfo As Boolean
            objFileDrawingInfo = System.IO.File.Exists(SprSupportPathString & "\LastApproval.txt")
            If objFileDrawingInfo = True Then
                'do nothing
            Else
                'drawing info file does not exist
                MsgBox("Last drawing number file does not exist")
                Return ""
                Exit Function
            End If
            'Read data from info file
            Dim InfoTxtFile As New System.IO.StreamReader(SprSupportPathString & "\LastApproval.txt")
            mydraw = InfoTxtFile.ReadLine()
            InfoTxtFile.Close()
            Return mydraw
        End Function

    End Class

End Namespace