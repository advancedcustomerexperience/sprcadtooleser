﻿Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.AutoCAD.DatabaseServices


Public Class frmStartUp
    Public NewDrawing As String
    Public AddDrawing As String
    Public ApproveDrawing As String
    Public SignoffRevision As String
    Public OpenDrawing As String
    Public MoveAppFile As String
    Public UpdateFiles As String
    Public CloseDrawing As String
    Public CopyLyout As String
    Public IQApproval As String
    Public IQOpenDrawing As String
    Public IQCopyDrawing As String


    Private Sub btnNewDrawing_Click(sender As Object, e As EventArgs) Handles btnNewDrawing.Click
        NewDrawing = "Yes"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    Private Sub btnAddDrawing_Click(sender As Object, e As EventArgs) Handles btnAddDrawing.Click
        NewDrawing = "No"
        AddDrawing = "Yes"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    Private Sub btnApproveDrawing_Click(sender As Object, e As EventArgs) Handles btnApproveDrawing.Click

        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "Yes"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    Private Sub btnRevision_Click(sender As Object, e As EventArgs) Handles btnRevision.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "Yes"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    Private Sub btnOpenDrawing_Click(sender As Object, e As EventArgs) Handles btnOpenDrawing.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "Yes"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    'Private Sub btnUpdateTitle_Click(sender As Object, e As EventArgs) Handles btnUpdateTitle.Click
    '    NewDrawing = "No"
    '    AddDrawing = "No"
    '    ApproveDrawing = "No"
    '    SignoffRevision = "No"
    '    OpenDrawing = "No"
    '    UpdateTitle = "Yes"
    '    UpdateFiles = "No"
    '    CloseDrawing = "No"
    '    CopyLyout = "No"
    '    IQApproval = "No"
    '    IQOpenDrawing = "No"
    '    IQCopyDrawing = "No"
    'End Sub

    Private Sub btnUpdateFiles_Click(sender As Object, e As EventArgs) Handles btnUpdateFiles.Click
        upDateCadFiles()
    End Sub

    Private Sub btnCloseDrawing_Click(sender As Object, e As EventArgs) Handles btnCloseDrawing.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "Yes"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub

    Private Sub btnCopyLayout_Click(sender As Object, e As EventArgs) Handles btnCopyLayout.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "Yes"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub
    Private Sub btnIQApproval_Click(sender As Object, e As EventArgs) Handles btnIQApproval.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "Yes"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub
    Private Sub btnOpenIQDrawing_Click(sender As Object, e As EventArgs) Handles btnOpenIQDrawing.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "Yes"
        IQCopyDrawing = "No"
    End Sub
    Private Sub btnCopyIQDrawing_Click(sender As Object, e As EventArgs) Handles btnCopyIQDrawing.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "No"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "Yes"
    End Sub

    Private Sub btnElevation_Click(ByVal sender As Object, e As EventArgs) Handles btnElevation.Click
        Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
        Dim mydoc As Document = Application.DocumentManager.MdiActiveDocument
        Using mydoc.LockDocument()
            Dim acLayoutMgr As LayoutManager
            acLayoutMgr = LayoutManager.Current
            acLayoutMgr.CurrentLayout = "Model"
            cdoc.SendStringToExecute("(c:elev) ", True, False, False)
        End Using
    End Sub

    Private Sub btnDecDim_Click(ByVal sender As Object, e As EventArgs) Handles btnDecDim.Click
        Application.SetSystemVariable("DIMLUNIT", 2)
        Application.SetSystemVariable("DIMDEC", 3)
    End Sub

    Private Sub btnFracDim_Click(ByVal sender As Object, e As EventArgs) Handles btnFracDim.Click
        Application.SetSystemVariable("DIMLUNIT", 5)
        Application.SetSystemVariable("DIMDEC", 6)
        Application.SetSystemVariable("DIMFRAC", 2)
    End Sub

    Private Sub btnArchDim_Click(ByVal sender As Object, e As EventArgs) Handles btnArchDim.Click
        Application.SetSystemVariable("DIMLUNIT", 4)
        Application.SetSystemVariable("DIMDEC", 6)
        Application.SetSystemVariable("DIMFRAC", 2)
    End Sub

    Private Sub btnScale_Click(ByVal sender As Object, e As EventArgs) Handles btnScale.Click
        Dim mySelectScale As New frmSelectScale
        Application.ShowModalDialog(mySelectScale)
    End Sub
    Public Sub upDateCadFiles()
        Dim SourcePath As String = ""
        Dim TargetPath As String = "C:\ProgramData\Autodesk"
        Dim FileName As String = ""
        Dim destFile As String = ""
        Dim folderName As String = ""
        Dim totalcount As Integer = 0
        Dim count As Integer = 0
        If (System.IO.Directory.Exists(TargetPath)) Then
            'do nothing, already exist
        Else
            System.IO.Directory.CreateDirectory(TargetPath)
        End If

        'Copy Lisp files from network to local support files on C: drive
        SourcePath = "\\innovation\CAD_Custom\Published\E-Series SPR CAD Tool\E_Series Lisp Programs"
        TargetPath = "C:\ProgramData\Autodesk\E_Series Lisp Programs"
        If (System.IO.Directory.Exists(SourcePath)) Then
            If (System.IO.Directory.Exists(TargetPath)) Then
                'do nothing
            Else
                System.IO.Directory.CreateDirectory(TargetPath)
            End If

            System.IO.Directory.GetAccessControl(TargetPath)

            totalcount = System.IO.Directory.GetFiles(SourcePath).Length
            count = 0
            ProgressBar.Visible = True

            ProgressBar.Maximum = totalcount

            Dim file = System.IO.Directory.GetFiles(SourcePath)
            For Each f In file
                FileName = System.IO.Path.GetFileName(f)
                destFile = System.IO.Path.Combine(TargetPath, FileName)
                System.IO.File.Copy(f, destFile, True)
                count = count + 1
                ProgressBar.Increment(1)
            Next
            MsgBox("Lisp files have been copied")
            ProgressBar.Value = 0
            ProgressBar.Visible = False

        End If
        'Copy Template files from network to local support files on C: drive
        SourcePath = "\\innovation\CAD_Custom\Published\E-Series SPR CAD Tool\Templates"
        TargetPath = "C:\ProgramData\Autodesk\AutoCAD Templates"
        totalcount = System.IO.Directory.GetFiles(SourcePath).Length
        count = 0
        ProgressBar.Visible = True

        ProgressBar.Maximum = totalcount

        If (System.IO.Directory.Exists(SourcePath)) Then
            If (System.IO.Directory.Exists(TargetPath)) Then
                'do nothing
            Else
                System.IO.Directory.CreateDirectory(TargetPath)
            End If
            Dim file = System.IO.Directory.GetFiles(SourcePath)
            For Each f In file
                FileName = System.IO.Path.GetFileName(f)
                destFile = System.IO.Path.Combine(TargetPath, FileName)
                System.IO.File.Copy(f, destFile, True)
                count = count + 1
                ProgressBar.Increment(1)
            Next
            MsgBox("Template files have been copied")
            ProgressBar.Value = 0
            ProgressBar.Visible = False
        End If

        'Copy Template files from network to local support files on C: drive
        'SourcePath = "R:\Engineering - Eagle\Public\SPR Support Files\SPR Support Files"
        TargetPath = "C:\ProgramData\Autodesk\SPR Support Files"
        'totalcount = System.IO.Directory.GetFiles(SourcePath).Length
        'count = 0
        'ProgressBar.Visible = True

        'ProgressBar.Maximum = totalcount

        'If (System.IO.Directory.Exists(SourcePath)) Then
        '    If (System.IO.Directory.Exists(TargetPath)) Then
        '        'do nothing
        '    Else
        '        System.IO.Directory.CreateDirectory(TargetPath)
        '    End If
        '    Dim file = System.IO.Directory.GetFiles(SourcePath)
        '    For Each f In file
        '        FileName = System.IO.Path.GetFileName(f)
        '        destFile = System.IO.Path.Combine(TargetPath, FileName)
        '        System.IO.File.Copy(f, destFile, True)
        '        count = count + 1
        '        ProgressBar.Increment(1)
        '    Next
        '    MsgBox("Support files have been copied")
        '    ProgressBar.Value = 0
        '    ProgressBar.Visible = False
        'End If

        If (System.IO.Directory.Exists(TargetPath)) Then
            'do nothing
        Else
            System.IO.Directory.CreateDirectory(TargetPath)
        End If


        MsgBox("All files have been copied")
    End Sub

    'Private Sub frmStartUp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    If System.Diagnostics.Debugger.IsAttached = False Then
    '        Me.Text = "E-Series SPR CAD Tool: " &
    '        System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()

    '    Else
    '        Me.Text = "Debug Mode"
    '    End If
    'End Sub

    Private Sub btnCopyIQApproval_Click(sender As Object, e As EventArgs) Handles btnCopyIQApproval.Click

    End Sub

    Private Sub btnMoveApprovedFile_Click(sender As Object, e As EventArgs) Handles btnMoveApprovedFile.Click
        NewDrawing = "No"
        AddDrawing = "No"
        ApproveDrawing = "No"
        SignoffRevision = "No"
        OpenDrawing = "No"
        MoveAppFile = "Yes"
        UpdateFiles = "No"
        CloseDrawing = "No"
        CopyLyout = "No"
        IQApproval = "No"
        IQOpenDrawing = "No"
        IQCopyDrawing = "No"
    End Sub
End Class