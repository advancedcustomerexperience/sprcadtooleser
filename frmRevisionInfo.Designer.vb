﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRevisionInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtRevisionLevel = New System.Windows.Forms.TextBox()
        Me.txtSalesforce = New System.Windows.Forms.TextBox()
        Me.txrRevDes = New System.Windows.Forms.TextBox()
        Me.txtDrafter = New System.Windows.Forms.TextBox()
        Me.txtRevisionDate = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnContinue = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtRevisionLevel
        '
        Me.txtRevisionLevel.Enabled = False
        Me.txtRevisionLevel.Location = New System.Drawing.Point(112, 12)
        Me.txtRevisionLevel.Name = "txtRevisionLevel"
        Me.txtRevisionLevel.Size = New System.Drawing.Size(38, 20)
        Me.txtRevisionLevel.TabIndex = 0
        '
        'txtSalesforce
        '
        Me.txtSalesforce.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalesforce.Location = New System.Drawing.Point(112, 91)
        Me.txtSalesforce.Name = "txtSalesforce"
        Me.txtSalesforce.Size = New System.Drawing.Size(92, 20)
        Me.txtSalesforce.TabIndex = 3
        '
        'txrRevDes
        '
        Me.txrRevDes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txrRevDes.Location = New System.Drawing.Point(12, 141)
        Me.txrRevDes.Multiline = True
        Me.txrRevDes.Name = "txrRevDes"
        Me.txrRevDes.Size = New System.Drawing.Size(192, 58)
        Me.txrRevDes.TabIndex = 4
        '
        'txtDrafter
        '
        Me.txtDrafter.Enabled = False
        Me.txtDrafter.Location = New System.Drawing.Point(112, 39)
        Me.txtDrafter.Name = "txtDrafter"
        Me.txtDrafter.Size = New System.Drawing.Size(56, 20)
        Me.txtDrafter.TabIndex = 1
        '
        'txtRevisionDate
        '
        Me.txtRevisionDate.Enabled = False
        Me.txtRevisionDate.Location = New System.Drawing.Point(112, 65)
        Me.txtRevisionDate.Name = "txtRevisionDate"
        Me.txtRevisionDate.Size = New System.Drawing.Size(92, 20)
        Me.txtRevisionDate.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Revision Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "OTT #"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(48, 125)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Revsion Description"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(63, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Drafter"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(34, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Revison Date"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnContinue
        '
        Me.btnContinue.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnContinue.Location = New System.Drawing.Point(27, 217)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.Size = New System.Drawing.Size(75, 23)
        Me.btnContinue.TabIndex = 5
        Me.btnContinue.Text = "&Continue"
        Me.btnContinue.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(112, 217)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Ca&ncel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmRevisionInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(219, 253)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnContinue)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtRevisionDate)
        Me.Controls.Add(Me.txtDrafter)
        Me.Controls.Add(Me.txrRevDes)
        Me.Controls.Add(Me.txtSalesforce)
        Me.Controls.Add(Me.txtRevisionLevel)
        Me.Name = "frmRevisionInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRevisionInfo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRevisionLevel As System.Windows.Forms.TextBox
    Friend WithEvents txtSalesforce As System.Windows.Forms.TextBox
    Friend WithEvents txrRevDes As System.Windows.Forms.TextBox
    Friend WithEvents txtDrafter As System.Windows.Forms.TextBox
    Friend WithEvents txtRevisionDate As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnContinue As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
