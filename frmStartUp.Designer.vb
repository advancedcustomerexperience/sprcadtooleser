﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStartUp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnIQApproval = New System.Windows.Forms.Button()
        Me.btnCopyIQApproval = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnElevation = New System.Windows.Forms.Button()
        Me.btnArchDim = New System.Windows.Forms.Button()
        Me.btnScale = New System.Windows.Forms.Button()
        Me.btnFracDim = New System.Windows.Forms.Button()
        Me.btnDecDim = New System.Windows.Forms.Button()
        Me.btnCopyLayout = New System.Windows.Forms.Button()
        Me.btnCloseDrawing = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnUpdateFiles = New System.Windows.Forms.Button()
        Me.btnMoveApprovedFile = New System.Windows.Forms.Button()
        Me.btnOpenDrawing = New System.Windows.Forms.Button()
        Me.btnRevision = New System.Windows.Forms.Button()
        Me.btnApproveDrawing = New System.Windows.Forms.Button()
        Me.btnAddDrawing = New System.Windows.Forms.Button()
        Me.btnNewDrawing = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnCopyIQDrawing = New System.Windows.Forms.Button()
        Me.btnOpenIQDrawing = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnIQApproval
        '
        Me.btnIQApproval.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnIQApproval.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIQApproval.Location = New System.Drawing.Point(11, 20)
        Me.btnIQApproval.Name = "btnIQApproval"
        Me.btnIQApproval.Size = New System.Drawing.Size(90, 53)
        Me.btnIQApproval.TabIndex = 16
        Me.btnIQApproval.Text = "IQ Drawing Approval"
        Me.btnIQApproval.UseVisualStyleBackColor = True
        '
        'btnCopyIQApproval
        '
        Me.btnCopyIQApproval.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCopyIQApproval.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopyIQApproval.Location = New System.Drawing.Point(11, 203)
        Me.btnCopyIQApproval.Name = "btnCopyIQApproval"
        Me.btnCopyIQApproval.Size = New System.Drawing.Size(90, 53)
        Me.btnCopyIQApproval.TabIndex = 17
        Me.btnCopyIQApproval.Text = "Copy IQ Drawing Approval"
        Me.btnCopyIQApproval.UseVisualStyleBackColor = True
        Me.btnCopyIQApproval.Visible = False
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(23, 358)
        Me.ProgressBar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(474, 23)
        Me.ProgressBar.TabIndex = 19
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnElevation)
        Me.GroupBox1.Controls.Add(Me.btnArchDim)
        Me.GroupBox1.Controls.Add(Me.btnScale)
        Me.GroupBox1.Controls.Add(Me.btnFracDim)
        Me.GroupBox1.Controls.Add(Me.btnDecDim)
        Me.GroupBox1.Controls.Add(Me.btnCopyLayout)
        Me.GroupBox1.Controls.Add(Me.btnCloseDrawing)
        Me.GroupBox1.Controls.Add(Me.btnCancel)
        Me.GroupBox1.Controls.Add(Me.btnUpdateFiles)
        Me.GroupBox1.Controls.Add(Me.btnMoveApprovedFile)
        Me.GroupBox1.Controls.Add(Me.btnOpenDrawing)
        Me.GroupBox1.Controls.Add(Me.btnRevision)
        Me.GroupBox1.Controls.Add(Me.btnApproveDrawing)
        Me.GroupBox1.Controls.Add(Me.btnAddDrawing)
        Me.GroupBox1.Controls.Add(Me.btnNewDrawing)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 8)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(335, 333)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "EV Commands"
        '
        'btnElevation
        '
        Me.btnElevation.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnElevation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnElevation.Location = New System.Drawing.Point(125, 270)
        Me.btnElevation.Name = "btnElevation"
        Me.btnElevation.Size = New System.Drawing.Size(90, 53)
        Me.btnElevation.TabIndex = 24
        Me.btnElevation.Text = "Elevation Programs"
        Me.btnElevation.UseVisualStyleBackColor = True
        '
        'btnArchDim
        '
        Me.btnArchDim.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnArchDim.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnArchDim.Location = New System.Drawing.Point(232, 148)
        Me.btnArchDim.Name = "btnArchDim"
        Me.btnArchDim.Size = New System.Drawing.Size(90, 53)
        Me.btnArchDim.TabIndex = 27
        Me.btnArchDim.Text = "Architecural Dimension"
        Me.btnArchDim.UseVisualStyleBackColor = True
        '
        'btnScale
        '
        Me.btnScale.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnScale.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScale.Location = New System.Drawing.Point(232, 209)
        Me.btnScale.Name = "btnScale"
        Me.btnScale.Size = New System.Drawing.Size(90, 53)
        Me.btnScale.TabIndex = 28
        Me.btnScale.Text = "Scale Layout"
        Me.btnScale.UseVisualStyleBackColor = True
        '
        'btnFracDim
        '
        Me.btnFracDim.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnFracDim.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFracDim.Location = New System.Drawing.Point(232, 87)
        Me.btnFracDim.Name = "btnFracDim"
        Me.btnFracDim.Size = New System.Drawing.Size(90, 53)
        Me.btnFracDim.TabIndex = 26
        Me.btnFracDim.Text = "Fractional Dimension"
        Me.btnFracDim.UseVisualStyleBackColor = True
        '
        'btnDecDim
        '
        Me.btnDecDim.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnDecDim.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDecDim.Location = New System.Drawing.Point(232, 26)
        Me.btnDecDim.Name = "btnDecDim"
        Me.btnDecDim.Size = New System.Drawing.Size(90, 53)
        Me.btnDecDim.TabIndex = 25
        Me.btnDecDim.Text = "Decimal Dimension"
        Me.btnDecDim.UseVisualStyleBackColor = True
        '
        'btnCopyLayout
        '
        Me.btnCopyLayout.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCopyLayout.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopyLayout.Location = New System.Drawing.Point(125, 26)
        Me.btnCopyLayout.Name = "btnCopyLayout"
        Me.btnCopyLayout.Size = New System.Drawing.Size(90, 53)
        Me.btnCopyLayout.TabIndex = 20
        Me.btnCopyLayout.Text = "Copy Layout"
        Me.btnCopyLayout.UseVisualStyleBackColor = True
        '
        'btnCloseDrawing
        '
        Me.btnCloseDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCloseDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseDrawing.Location = New System.Drawing.Point(17, 270)
        Me.btnCloseDrawing.Name = "btnCloseDrawing"
        Me.btnCloseDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnCloseDrawing.TabIndex = 19
        Me.btnCloseDrawing.Text = "&Close Drawing"
        Me.btnCloseDrawing.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(232, 270)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(90, 53)
        Me.btnCancel.TabIndex = 29
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnUpdateFiles
        '
        Me.btnUpdateFiles.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnUpdateFiles.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateFiles.Location = New System.Drawing.Point(125, 209)
        Me.btnUpdateFiles.Name = "btnUpdateFiles"
        Me.btnUpdateFiles.Size = New System.Drawing.Size(90, 53)
        Me.btnUpdateFiles.TabIndex = 23
        Me.btnUpdateFiles.Text = "Update Support Files"
        Me.btnUpdateFiles.UseVisualStyleBackColor = True
        '
        'btnMoveApprovedFile
        '
        Me.btnMoveApprovedFile.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnMoveApprovedFile.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoveApprovedFile.Location = New System.Drawing.Point(125, 87)
        Me.btnMoveApprovedFile.Name = "btnMoveApprovedFile"
        Me.btnMoveApprovedFile.Size = New System.Drawing.Size(90, 53)
        Me.btnMoveApprovedFile.TabIndex = 21
        Me.btnMoveApprovedFile.Text = "Move Approved File"
        Me.btnMoveApprovedFile.UseVisualStyleBackColor = True
        '
        'btnOpenDrawing
        '
        Me.btnOpenDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOpenDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenDrawing.Location = New System.Drawing.Point(17, 209)
        Me.btnOpenDrawing.Name = "btnOpenDrawing"
        Me.btnOpenDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnOpenDrawing.TabIndex = 18
        Me.btnOpenDrawing.Text = "&Open Drawing"
        Me.btnOpenDrawing.UseVisualStyleBackColor = True
        '
        'btnRevision
        '
        Me.btnRevision.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnRevision.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRevision.Location = New System.Drawing.Point(125, 148)
        Me.btnRevision.Name = "btnRevision"
        Me.btnRevision.Size = New System.Drawing.Size(90, 53)
        Me.btnRevision.TabIndex = 22
        Me.btnRevision.Text = " Signoff Revision"
        Me.btnRevision.UseVisualStyleBackColor = True
        '
        'btnApproveDrawing
        '
        Me.btnApproveDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnApproveDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApproveDrawing.Location = New System.Drawing.Point(17, 148)
        Me.btnApproveDrawing.Name = "btnApproveDrawing"
        Me.btnApproveDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnApproveDrawing.TabIndex = 17
        Me.btnApproveDrawing.Text = "EV Drawing Approval"
        Me.btnApproveDrawing.UseVisualStyleBackColor = True
        '
        'btnAddDrawing
        '
        Me.btnAddDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAddDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddDrawing.Location = New System.Drawing.Point(17, 87)
        Me.btnAddDrawing.Name = "btnAddDrawing"
        Me.btnAddDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnAddDrawing.TabIndex = 16
        Me.btnAddDrawing.Text = "Add Customer Signoff"
        Me.btnAddDrawing.UseVisualStyleBackColor = True
        '
        'btnNewDrawing
        '
        Me.btnNewDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnNewDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNewDrawing.Location = New System.Drawing.Point(17, 26)
        Me.btnNewDrawing.Name = "btnNewDrawing"
        Me.btnNewDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnNewDrawing.TabIndex = 15
        Me.btnNewDrawing.Text = "New Customer Signoff"
        Me.btnNewDrawing.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnCopyIQDrawing)
        Me.GroupBox2.Controls.Add(Me.btnOpenIQDrawing)
        Me.GroupBox2.Controls.Add(Me.btnIQApproval)
        Me.GroupBox2.Controls.Add(Me.btnCopyIQApproval)
        Me.GroupBox2.Location = New System.Drawing.Point(353, 14)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox2.Size = New System.Drawing.Size(143, 327)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "IQ Drawing Commands"
        '
        'btnCopyIQDrawing
        '
        Me.btnCopyIQDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCopyIQDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopyIQDrawing.Location = New System.Drawing.Point(11, 80)
        Me.btnCopyIQDrawing.Name = "btnCopyIQDrawing"
        Me.btnCopyIQDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnCopyIQDrawing.TabIndex = 19
        Me.btnCopyIQDrawing.Text = "Copy Existing IQ Approval Drawing"
        Me.btnCopyIQDrawing.UseVisualStyleBackColor = True
        '
        'btnOpenIQDrawing
        '
        Me.btnOpenIQDrawing.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOpenIQDrawing.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenIQDrawing.Location = New System.Drawing.Point(11, 143)
        Me.btnOpenIQDrawing.Name = "btnOpenIQDrawing"
        Me.btnOpenIQDrawing.Size = New System.Drawing.Size(90, 53)
        Me.btnOpenIQDrawing.TabIndex = 18
        Me.btnOpenIQDrawing.Text = "Open IQ Drawing"
        Me.btnOpenIQDrawing.UseVisualStyleBackColor = True
        '
        'frmStartUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(536, 402)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProgressBar)
        Me.Name = "frmStartUp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "E-Series SPR Cad Tool"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnIQApproval As System.Windows.Forms.Button
    Friend WithEvents btnCopyIQApproval As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnElevation As System.Windows.Forms.Button
    Friend WithEvents btnArchDim As System.Windows.Forms.Button
    Friend WithEvents btnScale As System.Windows.Forms.Button
    Friend WithEvents btnFracDim As System.Windows.Forms.Button
    Friend WithEvents btnDecDim As System.Windows.Forms.Button
    Friend WithEvents btnCopyLayout As System.Windows.Forms.Button
    Friend WithEvents btnCloseDrawing As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnUpdateFiles As System.Windows.Forms.Button
    Friend WithEvents btnMoveApprovedFile As System.Windows.Forms.Button
    Friend WithEvents btnOpenDrawing As System.Windows.Forms.Button
    Friend WithEvents btnRevision As System.Windows.Forms.Button
    Friend WithEvents btnApproveDrawing As System.Windows.Forms.Button
    Friend WithEvents btnAddDrawing As System.Windows.Forms.Button
    Friend WithEvents btnNewDrawing As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnOpenIQDrawing As System.Windows.Forms.Button
    Friend WithEvents btnCopyIQDrawing As System.Windows.Forms.Button
End Class
