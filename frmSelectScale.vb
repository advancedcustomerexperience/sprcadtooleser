﻿
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.AutoCAD.DatabaseServices


Public Class frmSelectScale
    Dim strDrawingScale As String = ""



    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim cdoc As Document = Application.DocumentManager.MdiActiveDocument
        If Me.optSC1.Checked = True Then  '6" = 1'-0"
            strDrawingScale = "6""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.500xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC2.Checked = True Then '3" = 1'-0"
            strDrawingScale = "3""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.25xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC3.Checked = True Then '2" = 1'-0"
            strDrawingScale = "2""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.1666666666xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC4.Checked = True Then '1 1/2" = 1'-0"
            strDrawingScale = "1 1/2""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.125xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC5.Checked = True Then '1" = 1'-0"
            strDrawingScale = "1""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.08333333333xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC6.Checked = True Then '3/4" = 1'-0"
            strDrawingScale = "3/4""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.0625xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC7.Checked = True Then '1/2" = 1'-0"
            strDrawingScale = "1/2""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.04166666666xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC8.Checked = True Then '3/8" = 1'-0"
            strDrawingScale = "3/8""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.03125xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC9.Checked = True Then '5/16" = 1'-0"
            strDrawingScale = "5/16""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.026xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        ElseIf Me.optSC10.Checked = True Then '1/4" = 1'-0"
            strDrawingScale = "1/4""=1'-0"""
            cdoc.SendStringToExecute("_mspace ", True, False, False)
            cdoc.SendStringToExecute("_zoom 0.0208333333xp pspace ", True, False, False)
            SetScaleText(strDrawingScale)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub
    Public Sub SetScaleText(ByVal ScaleString As String)
        Dim doc As Document = Application.DocumentManager.MdiActiveDocument
        Dim acLayoutMgr As LayoutManager
        acLayoutMgr = LayoutManager.Current
        Dim myCurrLayout As String = acLayoutMgr.CurrentLayout
        Using doc.LockDocument()
            Dim myDB As Database = HostApplicationServices.WorkingDatabase
            Using myTrans As Transaction = myDB.TransactionManager.StartTransaction()
                Dim myBlockTable As BlockTable = myDB.BlockTableId.GetObject(OpenMode.ForRead)
                For Each myBTRid As ObjectId In myBlockTable
                    Dim myBlockTableRecord As BlockTableRecord = myBTRid.GetObject(OpenMode.ForRead)
                    If myBlockTableRecord.IsLayout = True Then
                        Dim myLayout As Layout = myBlockTableRecord.LayoutId.GetObject(OpenMode.ForRead)
                        Debug.IndentLevel = 1
                        'MsgBox(myLayout.LayoutName)
                        If myLayout.LayoutName = myCurrLayout Then
                            Dim strMyLayoutName As String = myLayout.LayoutName.ToUpper
                            For Each entID As ObjectId In myBlockTableRecord
                                Dim ent As Entity = TryCast(myTrans.GetObject(entID, OpenMode.ForRead), Entity)
                                If ent IsNot Nothing Then
                                    Dim br As BlockReference = TryCast(ent, BlockReference)
                                    If br IsNot Nothing Then
                                        Dim bd As BlockTableRecord = DirectCast(myTrans.GetObject(br.BlockTableRecord, OpenMode.ForRead), BlockTableRecord)  'to see whether it's a block with the name we're after
                                        If bd.Name = "SpecialsTitleBlock" Then               ' Check each of the attributes...
                                            For Each attReferenceId As ObjectId In br.AttributeCollection
                                                Dim obj As DBObject = myTrans.GetObject(attReferenceId, OpenMode.ForRead)
                                                Dim attReference As AttributeReference = TryCast(obj, AttributeReference)
                                                If attReference IsNot Nothing Then                    'to see whether it has the tag we're after
                                                    If attReference.Tag.ToUpper() = "SCALE" Then     ' If so, update the value and increment the counter
                                                        attReference.UpgradeOpen()
                                                        attReference.TextString = ScaleString
                                                        attReference.DowngradeOpen()
                                                    End If
                                                End If
                                            Next
                                        End If
                                        'UpdateAttributesInBlock(br.BlockTableRecord, "SpecialsTitleBlock", "Customer", "Stan")    ' Recurse for nested blocks
                                    End If
                                End If


                            Next
                        End If





                        ' MsgBox(myLayout.CanonicalMediaName)

                    End If
                    Debug.IndentLevel = 0
                Next
                myTrans.Commit()
            End Using
        End Using


    End Sub

End Class