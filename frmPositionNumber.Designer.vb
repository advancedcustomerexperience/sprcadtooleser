﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPositionNumber
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxPositionNum = New System.Windows.Forms.ComboBox()
        Me.btnContinue = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxLayoutTabs = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxDLOPosition = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'cbxPositionNum
        '
        Me.cbxPositionNum.AutoCompleteCustomSource.AddRange(New String() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"})
        Me.cbxPositionNum.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPositionNum.FormattingEnabled = True
        Me.cbxPositionNum.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"})
        Me.cbxPositionNum.Location = New System.Drawing.Point(41, 103)
        Me.cbxPositionNum.MaxDropDownItems = 20
        Me.cbxPositionNum.Name = "cbxPositionNum"
        Me.cbxPositionNum.Size = New System.Drawing.Size(62, 23)
        Me.cbxPositionNum.TabIndex = 1
        '
        'btnContinue
        '
        Me.btnContinue.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnContinue.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContinue.Location = New System.Drawing.Point(36, 134)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.Size = New System.Drawing.Size(67, 24)
        Me.btnContinue.TabIndex = 2
        Me.btnContinue.Text = "Continue"
        Me.btnContinue.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(109, 134)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(67, 24)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(41, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 45)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Select Unit Position"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cbxLayoutTabs
        '
        Me.cbxLayoutTabs.AutoCompleteCustomSource.AddRange(New String() {"Elevation", "Glass", "GrilleInt", "GrilleExt", "Parts"})
        Me.cbxLayoutTabs.FormattingEnabled = True
        Me.cbxLayoutTabs.Location = New System.Drawing.Point(51, 29)
        Me.cbxLayoutTabs.Name = "cbxLayoutTabs"
        Me.cbxLayoutTabs.Size = New System.Drawing.Size(121, 21)
        Me.cbxLayoutTabs.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(50, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(124, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Select Layout to Copy"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(113, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 45)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Select DLO Position"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cbxDLOPosition
        '
        Me.cbxDLOPosition.AutoCompleteCustomSource.AddRange(New String() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"})
        Me.cbxDLOPosition.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDLOPosition.FormattingEnabled = True
        Me.cbxDLOPosition.Items.AddRange(New Object() {"D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9"})
        Me.cbxDLOPosition.Location = New System.Drawing.Point(113, 103)
        Me.cbxDLOPosition.MaxDropDownItems = 20
        Me.cbxDLOPosition.Name = "cbxDLOPosition"
        Me.cbxDLOPosition.Size = New System.Drawing.Size(62, 23)
        Me.cbxDLOPosition.TabIndex = 6
        '
        'frmPositionNumber
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(216, 167)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbxDLOPosition)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxLayoutTabs)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnContinue)
        Me.Controls.Add(Me.cbxPositionNum)
        Me.Name = "frmPositionNumber"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Position Number"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxPositionNum As System.Windows.Forms.ComboBox
    Friend WithEvents btnContinue As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxLayoutTabs As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxDLOPosition As System.Windows.Forms.ComboBox
End Class
